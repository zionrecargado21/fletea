$(document).ready(
		function() {

			/* path de la aplicacion */
			var path = $(location).attr("origin") + "/fleteawebapp/"
			/* fin path de la aplicacion */

			/* variables de inputs */
			var provincia = $('#provincia');
			var localidad = $('#localidad');
			var barrio = $('#barrio');
			/* fin variables */

			/* Eventos */
			provincia.change(function() {
				getAllLocalidadesByProvincia(provincia, localidad);
			});

			localidad.change(function() {
				getAllBarriosByLocalidad(localidad, barrio);
			});
			/* Fin Eventos */

			/**
			 * Funcion que retorna todas las localidad por el id de provincia
			 */
			function getAllLocalidadesByProvincia(provincia, localidad) {

				/* variables */
				var idProvincia = provincia.val();

				$.post(path + "getLocalidadesByProvincia", {
					idProvincia : idProvincia
				}, function() {
					console.log("success");
				}).done(
						function(data) {
							console.log(data);
							var options = '<option value=""></option>';
							$.each(data, function(index, value) {
								options += '<option value="'
										+ value.idLocalidad + '">'
										+ value.nombre + '</option>';
							});

							localidad.empty();
							localidad.append(options);

						}).fail(function() {
					console.log("error");
				}).always(function() {
					console.log("finished");
				});

			}

			/**
			 * Funcion que retorna todas los barrios por el id de localidad
			 */
			function getAllBarriosByLocalidad(localidad, barrio) {

				/* variables */
				var idLocalidad = localidad.val();

				$.post(path + "getBarriosByLocalidad", {
					idLocalidad : idLocalidad
				}, function() {
					console.log("success");
				}).done(
						function(data) {
							console.log(data);
							var options = '<option value=""></option>';
							$.each(data, function(index, value) {
								options += '<option value="' + value.idBarrio
										+ '">' + value.nombre + '</option>';
							});

							barrio.empty();
							barrio.append(options);

						}).fail(function() {
					console.log("error");
				}).always(function() {
					console.log("finished");
				});

			}

			$(".cont-tabla").on(
					"click",
					"a.btn-disponibilidad",
					function(e) {
						var href = $(this).attr("href");
						$.get(href, function(data) {
							$('#myModal table tbody').html("");
							if(data.length == 0) {
								$('#myModal table tbody').append("<tr><td colspan='3'>No existe una disponibilidad</td></tr>");
							} else {
								for (var i = 0; i < data.length; i++) {
									$('#myModal table tbody').append(
											"" + "<tr>" + "<td>"
													+ data[i].dia.nombre + "</td>"
													+ "<td>" + data[i].horaDesde
													+ "</td>" + "<td>"
													+ data[i].horaHasta + "</td>"
													+ "</tr>");
								}	
							}
							
							$('#myModal').modal('show');
						});
						e.preventDefault();
					});
			
			
			
			
		    function cargarRankingFleteros(){
		    	$('#grafico').html('');
		    	var cant = $("#txtTopCant").val();
		         $.ajax({
		             url: 'RankingFleterosJSON?top='+ cant,
		             method: 'GET'
		         }).done(function(response){
		        	 var filas = "";
		        	 $.each(response, function(idx, opt) {
		        		 var col1 = '<td>'+ opt.cantServicios + '</td>';
		        		 var col2 = '<td>'+ opt.nombre + '</td>';
		        		 var col3 = '<td>'+ opt.localidad + '</td>';
		        		 var col4 = '<td>'+ opt.provincia + '</td>';
		        		 
		        		 filas = filas + '<tr>' + col1 + col2 + col3 + col4 + '</tr>';
		        		 
		        	 });
		        	 $('#tablaRankingFleteros tbody').html(filas);
		        	 
		        	 var grafData = [];
		        	 
		        	 for(i=0;i<response.length;i++){
		        		 grafData.push({label: response[i].nombre, value: response[i].cantServicios});
		        	 }
		  
		        	 
		        	 Morris.Donut({
		        		  element: 'grafico',
		        		  data: grafData
		        		});
		        	 
		         }).fail(function(response){
		         }); 
		    }
		    
		    $("#btnRankingFleteros").click(function(){
		    	cargarRankingFleteros();
		    });
			
		    function cargarServiciosConcretados(){
		    	var desde = $("#txtDesde").val().replace("-", "").replace("-", "");
		    	var hasta = $("#txtHasta").val().replace("-", "").replace("-", "");
		         $.ajax({
		             url: 'ServiciosConcretadosJSON?desde='+ desde +"&hasta="+ hasta,
		             method: 'GET'
		         }).done(function(response){
		        	 var filas = "";
		        	 $.each(response, function(idx, opt) {
		        		 var col1 = '<td>'+ opt.idServicio + '</td>';
		        		 var col2 = '<td>'+ opt.nombre + '</td>';
		        		 var col3 = '<td>'+ opt.tipoVehiculo + '</td>';
		        		 var col4 = '<td>'+ opt.localidad + '</td>';
		        		 var col5 = '<td>'+ opt.provincia + '</td>';
		        		 var col6 = '<td>'+ opt.fechaSolicitud + '</td>';
		        		 
		        		 filas = filas + '<tr>' + col1 + col2 + col3 + col4 + col5 + col6 + '</tr>';
		        		 
		        	 });
		        	 $('#tablaServiciosConcretados tbody').html(filas);
		         }).fail(function(response){
		         }); 
		    }
		    
		    $("#btnServiciosConcretados").click(function(){
		    	cargarServiciosConcretados();
		    });
			
			
			///////
			
		});