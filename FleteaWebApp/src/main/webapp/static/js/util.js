$(document).ready(function(){

	/* Time picker */
	$('.timepicker').timepicker({
	    timeFormat: 'h:mm p',
	    interval: 30,
	    minTime: '07',
	    maxTime: '10:00pm',
	    defaultTime: '11',
	    startTime: '07:00',
	    dynamic: false,
	    dropdown: true,
	    scrollbar: true
	});
	
	/* Bootstrap table */
//	$('#table').bootstrapTable({
//	    columns: [{
//	        field: 'id',
//	        title: 'Item ID'
//	    }, {
//	        field: 'name',
//	        title: 'Item Name'
//	    }, {
//	        field: 'price',
//	        title: 'Item Price'
//	    }],
//	    data: [{
//	        id: 1,
//	        name: 'Item 1',
//	        price: '$1'
//	    }, {
//	        id: 2,
//	        name: 'Item 2',
//	        price: '$2'
//	    }]
//	});
//	
})
