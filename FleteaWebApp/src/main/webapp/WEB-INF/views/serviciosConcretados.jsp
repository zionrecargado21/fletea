<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
			
			
					<div class="row" style="padding-top: 10px; padding-bottom: 10px"><h2>Servicios Concretados</h2></div>
					
					
					<div class="row" style="padding-top: 10px; padding-bottom: 10px">
						
						<div class="col-md-3">
							<div class="form-group">
								<label>Desde</label>
								<input autofocus="true" id="txtDesde" type="date" class="form-control" />
							</div>	
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Hasta</label>
								<input id="txtHasta" type="date" class="form-control" />
							</div>		
					
						</div>
							
					</div>		
				
				<div class="row" style="padding-top: 10px; padding-bottom: 20px"><button type="button" id="btnServiciosConcretados" class="btn btn-primary">Generar</button></div>
				
				<div class="row">
				
					
						
					<div class="table-responsive">
						<table id="tablaServiciosConcretados" class="table table-hover">					     
							<thead>			
							  <tr>
							     <td>Id Servicio</td>			
							     <td>Fletero</td>	
							     <td>Tipo Veh�culo</td>	
							     <td>Localidad</td>	
						         <td>Provincia</td>	
							     <td>Fecha Solicitud</td>					  	
							  </tr>				
							</thead>
								<tbody>
									
								</tbody>
						</table>
						</div>
						
			
				</div>
			</div>
		</header>
		
		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
		
		


	</tiles:putAttribute>
</tiles:insertDefinition>

