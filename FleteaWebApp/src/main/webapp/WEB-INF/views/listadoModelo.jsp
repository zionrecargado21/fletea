<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
			<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Listado Modelo</h3>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">
				<form action="guardarModelo" method="post"
		       modelAttribute="modelo">
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6">
									<table id="table"  data-pagination="true" data-page-size="5" data-search="true"
									data-toggle="table"  data-mobile-responsive="true"
                        mobileResponsive="true">
										<thead>
											<tr>
												<th data-sortable="true" data-field="id">ID</th>
												<th data-sortable="true" data-field="descripcion">Descripcion</th>
												<th data-sortable="true" data-field="marca">Marca</th>
												<th>Editar</th>
												<th>Eliminar</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listaModelo}" var="tmodelo" varStatus="loopCounter">
											
											     <tr>
													<td>${tmodelo.idModeloVehiculo}</td>
													<td>${tmodelo.descripcion}</td>
													<td>${tmodelo.marca}</td>
									 				<td><a href="editarModelo?idModelo=${tmodelo.idModeloVehiculo}">Editar</a></td>
									 				<td><a href="borrarModelo?idModelo=${tmodelo.idModeloVehiculo}">Borrar</a></td>
												</tr>
											
											</c:forEach>
										</tbody>
									</table>
									
								</div>
							</div>
							<div class="row mt-5">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<h4>
											<a href="registrarModelo" class="btn btn-primary"> Registrar nuevo</a>
										</h4>	
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>