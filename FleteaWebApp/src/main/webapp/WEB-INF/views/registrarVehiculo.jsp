<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-left">
						<div class="form-group">
							<label class="control-label col-sm-2" for="fletero">Fletero:</label>
							<div class="col-sm-10">
								<p class="form-control-static"><i class="fa fa-user-circle" aria-hidden="true"></i> ${fletero.nombre}</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Registraci�n Vehiculo</h3>
					</div>
				</div>
				<br>
				<form action="guardarVehiculo" method="post">
					<div class="row">
						<div class="col-md-12">
							<form action="#">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="tipoVehiculo">Tipo de Vehiculo</label>
											<form:select name="tipoVehiculo" id="tipoVehiculo"
												path="listadoTipoVehiculo" class="form-control"
												items="${listadoTipoVehiculo}" itemValue="idTipoVehiculo"
												itemLabel="descripcion" />
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="cCarga">Capacidad de Carga</label> <input
												type="text" name="capacidadCarga" class="form-control">
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="modeloVehiculo">Modelo Vehiculo</label>
											<form:select name="modeloVehiculo" id="modeloVehiculo"
												path="listadoModeloVehiculo" class="form-control"
												items="${listadoModeloVehiculo}"
												itemValue="idModeloVehiculo" itemLabel="descripcion" />
											</select>
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="anio">A�o</label> <select name="a�o" id="cAnio"
												class="form-control" required="true">
												<option value=""></option>
											</select>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="tarifaPorHora">Tarifa por hora</label> <input
												type="tarifaPorHora" name="tarifaPorHora"
												class="form-control">
										</div>
									</div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="patente">Patente</label> <input type="patente"
												name="patente" class="form-control">
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>

								<br>
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-6 text-center">
										<div class="form-group">
											<button type="" class="btn btn-primary">Registrar
												Vehiculo</button>
										</div>
									</div>
									<div class="col-md-3"></div>
								</div>
							</form>
						</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
		<script>
			(function() {
				for (i = new Date().getFullYear(); i > 1949; i--) {

					var x = document.getElementById("cAnio");
					var option = document.createElement("option");
					option.value = i;
					option.text = i;
					x.add(option);

				}
			})();
		</script>
	</tiles:putAttribute>
</tiles:insertDefinition>