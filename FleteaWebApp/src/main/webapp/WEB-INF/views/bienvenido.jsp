<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row mb-5">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
						<h2>Bienvenido ${usuario}!!</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="row mb-4">
							<div class="col-md-6">
								<a href="<c:url value = "listadoTipoVehiculo"/>" class="btn btn-success btn-block">Listado Tipo de Vehiculos</a>
							</div>
							<div class="col-md-6">
								<a href="<c:url value = "listadoModelo"/>" class="btn btn-success btn-block">Listado Modelos</a>
							</div>
						</div>
						<div class="row mb-4">
							<div class="col-md-6">
								<a href="<c:url value = "listadoClientes"/>" class="btn btn-success btn-block">Listado de Clientes</a>
							</div>
							<div class="col-md-6">
								<a href="<c:url value = "listadoFletero"/>" class="btn btn-success btn-block">Listado de Fleteros</a>
							</div>
						</div>
						<div class="row mb-4">
							<div class="col-md-6">
								<a href="<c:url value = "listadoMarcaVehiculo"/>" class="btn btn-success btn-block">Listado de Marcas</a>
							</div>
							<div class="col-md-6"></div>
						</div>
					</div>
					
				</div>
				
				
			</div>
		</header>
		
		
		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>

