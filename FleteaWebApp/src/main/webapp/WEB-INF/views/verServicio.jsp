<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<div class="row">
					<div class="col-lg-3"></div>
					<div class="col-lg-6">
						<div class="row">
							<div class="col-sm-12 text-center mt-5 mb-3">
								<h1>
									<i class="fa fa-cube" aria-hidden="true"></i> Informaci�n de
									Servicio
								</h1>
							</div>

							<div class="col-sm-12 card pb-4 mb-4" style="color: black;">
								<div class="card-body">
									<h4 class="card-title">
										<i class="fa fa-user-circle" aria-hidden="true"></i> Cliente
									</h4>
									<p class="card-text">
									<div class="form-group col-lg-12 text-center">
										<label>Nombre: ${servicio.cliente.nombre}</label>
									</div>
									</p>
								</div>

								<div class="card-body">
									<h4 class="card-title">
										<i class="fa fa-user-circle-o" aria-hidden="true"></i> Fletero
									</h4>
									<p class="card-text">
									<div class="form-group col-lg-12 text-center">
										<label>Nombre / Razon Social:
											${servicio.fletero.nombre}</label>
									</div>
									</p>
								</div>

								<div class="card">
									<div class="card-header">
										<i class="fa fa-truck" aria-hidden="true"></i> Veh�culo
									</div>
									<ul class="list-group list-group-flush">
										<li class="list-group-item">Tipo vehiculo:
											${servicio.vehiculo.tipoVehiculo.descripcion}</li>
										<li class="list-group-item">Tipo vehiculo:
											${servicio.vehiculo.tipoVehiculo.descripcion}</li>
										<li class="list-group-item">Patente:
											${servicio.vehiculo.patente}</li>
										<li class="list-group-item">Capacidad carga:
											${servicio.vehiculo.capacidadCarga}</li>
										<li class="list-group-item">Tarifa/Hora:
											${servicio.vehiculo.tarifaPorHora}</li>
									</ul>
								</div>
							</div>

							<h2 class="col-sm-12 text-center mb-3">Datos de entrega</h2>

							<div class="col-sm-12 card py-4 mb-4" style="color: black;">
								<div class="card-header">
									<i class="fa fa-cube" aria-hidden="true"></i> Servicio
								</div>
								<ul class="list-group list-group-flush">
									<li class="list-group-item">Fecha Servicio: 
									<fmt:formatDate var="fechaServicio" value="${servicio.fechaServicio}" pattern="HH:mm a dd-MM-yyyy" />
									${fechaServicio}
										</li>
									<li class="list-group-item">Barrio:
										${servicio.barrio.nombre}</li>
									<li class="list-group-item">Localidad:
										${servicio.barrio.localidad.nombre}</li>
									<li class="list-group-item">Provincia:
										${servicio.barrio.localidad.provincia.nombre}</li>
									<li class="list-group-item">Calle: ${servicio.calle}</li>
									<li class="list-group-item">Numero Calle:
										${servicio.numeroCalle}</li>
									<li class="list-group-item">Piso: ${servicio.numeroPiso}</li>
									<li class="list-group-item">Dpto.: ${servicio.dpto}</li>
									<li class="list-group-item">Descripcion:
										${servicio.descripcion}</li>
									<li class="list-group-item">Fecha Solicitud: 
										<fmt:formatDate var="fechaSolicitud"
									value="${servicio.fechaSolicitud}" pattern="HH:mm a dd-MM-yyyy" /> ${fechaSolicitud}</li>
									<li class="list-group-item">Trazabilidad:
										${trazabilidad.estadoServicio.descripcion} <c:choose>
											<c:when
												test="${trazabilidad.estadoServicio.idEstadoServicio == 1 && rolPersona == 2}">
												<a
													href="<c:url value = "/cambiarEstadoServicio?idEstado=2&idServicio=${servicio.idServicio}"/>"
													class="btn btn-success">Confirmar</a>
											</c:when>
										</c:choose> <c:choose>
											<c:when
												test="${trazabilidad.estadoServicio.idEstadoServicio < 3}">
												<a
													href="<c:url value = "/cambiarEstadoServicio?idEstado=3&idServicio=${servicio.idServicio}"/>"
													class="btn btn-danger">Cancelar</a>
											</c:when>
										</c:choose> <c:choose>
											<c:when
												test="${trazabilidad.estadoServicio.idEstadoServicio == 2 && rolPersona == 1}">
												<a
													href="<c:url value = "/cambiarEstadoServicio?idEstado=4&idServicio=${servicio.idServicio}"/>"
													class="btn btn-primary">Cerrar</a>
											</c:when>
										</c:choose> <c:choose>
											<c:when
												test="${trazabilidad.estadoServicio.idEstadoServicio == 4 && rolPersona == 1 && calificacionFletero == null}">
												<a
													href="<c:url value = "/calificarFletero?idServicio=${servicio.idServicio}"/>"
													class="btn btn-primary">Calificar fletero</a>
											</c:when>
										</c:choose> <c:choose>
											<c:when
												test="${trazabilidad.estadoServicio.idEstadoServicio == 4 && rolPersona == 2 && calificacionCliente == null}">
												<a
													href="<c:url value = "/calificarCliente?idServicio=${servicio.idServicio}"/>"
													class="btn btn-primary">Calificar cliente</a>
											</c:when>
										</c:choose>
									</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 text-center mb-3">
								<a
									href="<c:url value = "/misMensajes?idServicio=${servicio.idServicio}"/>"
									class="btn btn-primary"><i class="fa fa-paper-plane"
									aria-hidden="true"></i>Mensajes</a>
							</div>
						</div>
					</div>
				</div>
			</div>

		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>