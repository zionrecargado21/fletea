<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container h-100">
				<div class="row h-100">
					<div class="col-lg-7 my-auto">
						<div class="header-content mx-auto">
							<h1 class="mb-5">Con Flete�! realizar tus fletes es mucho
								mas facil, probalo es GRATIS!</h1>
							<a href="${contextPath}/login" class="btn btn-outline btn-xl">Comenz�
								ahora Gratis!</a>
						</div>
					</div>
					<div class="col-lg-5 my-auto">
						<div class="device-container">
							<div class="device-mockup iphone6_plus portrait white">
								<div class="device">
									<div class="screen">
										<!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
										<img src="static/img/fletaPhone.jpg" class="img-fluid" alt="">
									</div>
									<div class="button">
										<!-- You can hook the "home button" to some JavaScript events or just remove it -->
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<section class="download bg-primary text-center" id="download">
			<div class="container">
				<div class="row">
					<div class="col-md-8 mx-auto">
						<h2 class="section-heading">Encontra gran cantidad de
							opciones para realizar tu env�o !</h2>
						<p>Eleg� el que mejor se adapte a los que deseas transportar y
							a tu bolsillo!</p>
						<div class="badges">
							<a class="badge-link" href="#"><img
								src="img/google-play-badge.svg" alt=""></a> <a
								class="badge-link" href="#"><img
								src="img/app-store-badge.svg" alt=""></a>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="cta">
			<div class="cta-content">
				<div class="container">
					<h2>
						Las mejores opciones.<br>Al alcance de tu mano.
					</h2>
					<a href="${contextPath}/login" class="btn btn-outline btn-xl">Hagamos
						un flete!</a>
				</div>
			</div>
			<div class="overlay"></div>
		</section>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>