<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Servicios disponibles</h3>
					</div>
				</div>
				<div class="row mb-5 mt-4">
					<div class="col-md-12">
						<form class="row" action="filtrarBusqueda" method="post">
							<div class="form-group col-lg-6">
								<label for="nombreFletero">Nombre Fletero:</label> <input
									type="text" class="form-control" id="nombreFletero"
									placeholder="Ingrese un nombre del fletero"
									name="nombreFletero">
							</div>
							<div class="form-group col-lg-6">
								<label for="provincia">Provincia:</label>
								<form:select id="provincia" name="provincia"
									path="listaProvincia" class="form-control">
									<form:option value="">Seleccione una Provincia</form:option>
									<c:forEach items="${listaProvincia}" var="provincia">
										<form:option value="${provincia.idProvincia}">${provincia.nombre}</form:option>
									</c:forEach>
								</form:select>
							</div>
							<div class="form-group col-lg-6">
								<label for="tipoVehiculo">Tipo Vehiculo:</label>
								<form:select name="tipoVehiculo" id="tipoVehiculo"
									path="listaTipoVehiculo" class="form-control"
									items="${listaTipoVehiculo}" itemValue="idTipoVehiculo"
									itemLabel="descripcion" />
								</select>
							</div>
							<div class="form-group col-lg-6">
								<label for="localidad">Localidad:</label> <select id="localidad"
									class="form-control" name="localidad" required="true">
									<option value=""></option>
								</select>
							</div>
							<div class="form-group col-lg-6"></div>
							<div class="form-group col-lg-6">
								<label for="barrio">Barrio:</label> <select id="barrio"
									class="form-control" name="barrio" required="true">
									<option value=""></option>
								</select>
							</div>
							<div class="col-sm-12">
								<button type="submit" class="btn btn-default">Buscar</button>

							</div>
						</form>
					</div>
				</div>
				<div class="row">

					<div class="col-md-12">
						<div class="row">
							<div class="col-sm-12 cont-tabla">
								<table id="table" data-pagination="true" data-search="true"
									data-toggle="table" data-page-size="5">
									<thead>
										<tr>
											<th data-sortable="true" data-field="nombre">Nombre
												Fletero</th>
											<th data-sortable="true" data-field="vehiculo">Vehiculo</th>
											<th data-sortable="true" data-field="tarifa">TarifaPorHora</th>
											<th data-sortable="true" data-field="reputacion">Reputacion</th>
											<th data-sortable="true" data-field="disponibilidad">Disponibilidad</th>
											<th data-sortable="true" data-field="acciones">Acciones</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${listaVehiculo}" var="lista"
											varStatus="loopCounter">

											<tr>
												<td>${lista.vehiculo.fletero.nombre}</td>
												<td>${lista.vehiculo.tipoVehiculo.descripcion}</td>
												<td>${lista.vehiculo.tarifaPorHora}</td>
												<td>${lista.reputacion}</td>
												<td><a
													href="<c:url value = "/getDisponibilidadFletero?idFletero=${lista.vehiculo.fletero.idPersona}"/>"
													class="btn-disponibilidad">Ver </a></td>
												<td><a
													href="<c:url value = "/solicitudServicio?idVehiculo=${lista.vehiculo.idVehiculo}"/>">RegistrarPedido</a>
												</td>
											</tr>

										</c:forEach>
									</tbody>
								</table>

							</div>
						</div>
						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6 text-center"></div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Disponibilidad</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<table style="width: 100%;">
							<thead>
								<tr>
									<th>Dia</th>
									<th>Desde</th>
									<th>Hasta</th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Aceptar</button>
					</div>
				</div>
			</div>
		</div>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>