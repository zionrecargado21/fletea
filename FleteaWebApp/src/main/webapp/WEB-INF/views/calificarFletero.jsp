<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center mt-5 mb-4">
						<h3>Calificar Servicio</h3>
					</div>
				</div>

				<form:form action="guardarCalificacionFletero" method="post"
					class="row" modelAttribute="calificacion">

					<div class="col-md-3"></div>
					<div class="col-md-6 text-center">
						<div class="form-group">
							<label for="nombre">Calificación</label>
							<form:input min="1" max="5" type="number" name="cantEstrellas" path="cantEstrellas" class="form-control" />
						</div>

						<div class="form-group">
							<label for="nombre">Comentarios</label>
							<form:textarea name="comentario" path="comentario" class="form-control" />
						</div>

						<div class="form-group">
							<button type="submit" class="btn btn-primary">Calificar</button>
						</div>
					</div>
					<input type="hidden" name="idServicio" value="${calificacion.servicio.idServicio}">
				</form:form>
			</div>
		</header>
		
		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>