<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row mb-5">
					<div class="col-md-2"></div>
					<div class="col-md-8 text-center">
						<h2>Bienvenido ${cliente.nombre}!!</h2>
					</div>
				</div>
				<div class="row mb-5">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6">Mail: ${cliente.mailUsuario}</div>
							<div class="col-md-6">Telefono: ${cliente.telefono}</div>
						</div>
						<div class="row">
							<div class="col-md-6">Provincia:
								${cliente.barrio.localidad.provincia.nombre}</div>
							<div class="col-md-6">Localidad:
								${cliente.barrio.localidad.nombre}</div>
						</div>
						<div class="row">
							<div class="col-md-6">Barrio: ${cliente.barrio.nombre}</div>
							<div class="col-md-6">Direccion: ${cliente.calle}
								${cliente.numeroCalle} ${cliente.numeroPiso}</div>
						</div>
					</div>
				</div>
				<div class="row mb-5">

					<div class="col-md-2"></div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-6 text-left">

								<a
									href="<c:url value = "editarCliente?idPersona=${cliente.idPersona}"/>"
									class="btn btn-success btn-block"><i class="fa fa-pencil"
									aria-hidden="true"></i>Mis Datos</a>

							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">

						<div class="row mb-5">
							<div class="col-md-12 text-left">
								<h3>Servicios:</h3>
								<table id="table" data-pagination="true" data-page-size="5" data-search="true"
									data-toggle="table" data-mobile-responsive="true"
									mobileResponsive="true">
									<thead>
										<th data-sortable="true" data-field="id">ID</th>
										<th data-sortable="true" data-field="cliente">Cliente</th>
										<th data-sortable="true" data-field="fechaSolicitud">Fecha
											de Solicitud</th>
										<th data-sortable="true" data-field="fechaServicio">Fecha
											de Servicio</th>
										<th>Ver Detalle Servicio</th>
									</thead>
									<tbody>
										<c:forEach items="${serviciosCliente}" var="servicios">
											<fmt:formatDate var="fechaServicio"
												value="${servicios.fechaServicio}"
												pattern="HH:mm a dd-MM-yyyy" />
											<fmt:formatDate var="fechaSolicitud"
												value="${servicios.fechaSolicitud}"
												pattern="HH:mm a dd-MM-yyyy" />
											<tr>
												<td>${servicios.idServicio}</td>
												<td>${servicios.cliente.nombre}</td>
												<td>${fechaSolicitud}</td>
												<td>${fechaServicio}</td>
												<td><a
													href="<c:url value = "/verServicio?idServicio=${servicios.idServicio}"/>"><i
														class="fa fa-eye" aria-hidden="true"></i></a></td>
											</tr>
										</c:forEach>

									</tbody>
								</table>
							</div>
						</div>

					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>