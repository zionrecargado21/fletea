<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="/WEB-INF/views/include.jsp"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<!-- 		<h3>Personas</h3> -->
		<%-- 		<c:forEach items="${model.personas}" var="persona"> --%>
		<%-- 			ID, Nombre: <c:out value="${persona.idPersona}" />, <c:out value="${persona.nombre}" /> --%>
		<!-- 			<br> -->
		<!-- 			<br> -->
		<%-- 		</c:forEach> --%>

		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						<div class="row ">
							<div class="col-md-12 text-center">
								<h2>Registro de Usuarios</h2>
								<br>
							</div>
						</div>
						<form action="#" class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="nombre">Nombre</label> <input type="text"
										name="nombre" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="apellido">Apellido</label> <input type="text"
										name="apellido" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="email">Email</label> <input type="email"
										name="email" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="password">Password</label> <input type="password"
										name="password" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="confirm_pass">Confirme Password</label> <input
										type="password" name="confirm_pass" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="direccion">Direcci�n</label> <input type="text"
										name="direccion" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="telefono">N�mero telefono</label> <input type="tel"
										name="telefono" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="provincia">Provincia</label> <select
										name="provincia" id="provincia" class="form-control">
										<option value="1">C�rdoba</option>
										<option value="2">Buenos Aires</option>
										<option value="2">Santiago del Estero</option>
									</select>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="cuit">DNI / CUIT</label> <input type="text"
										name="cuit" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="ciudad">Ciudad</label> <input type="text"
										name="ciudad" class="form-control">
								</div>
							</div>
							<div class="col-md-12 text-center">

								<button type="button" class="btn btn-primary">Crear
									Cuenta</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>