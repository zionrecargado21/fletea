<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
			<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Registrar Modelo Veh�culo</h3>
					</div>
				</div>
				<br>
				<form:form action="guardarModelo" method="post"
					modelAttribute="modelo">
					
					<form:hidden path="idModeloVehiculo" />
					<div class="row">
						<div class="col-md-12">
							<form action="#">
								<div class="row">
									<div class="col-md-3"></div>
									<div class="col-md-3">
										<div class="form-group">
											<label for="descripcion">Descripci�n Modelo</label> 
											<form:input path="descripcion" name="descripcion" class="form-control"/>
										</div>
									</div>
									<div class="col-md-3">
									
										<label for="marca">Marca</label> 
										<form:select path="marca.idMarca" 
										class="form-control"
										items="${allMarcas}" itemValue="idMarca" itemLabel="descripcion" >
										</form:select>
									</div>
									<div class="col-md-3"></div>
								</div>
				</form:form>
				<br>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<button type="" class="btn btn-primary">Registrar
											Modelo Vehiculo</button>
									</div>
								</div>
								<div class="col-md-3"></div>
							</div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<h4>										
											<a href="listadoModelo" class="btn btn-info"> Volver al listado</a>
										</h4>	
									</div>
								</div>
							</div>				
				</form>
			</div>
			</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>