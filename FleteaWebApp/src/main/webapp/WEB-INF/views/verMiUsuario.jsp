<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container" style="backgroud-color:white;">
				<br>
				<div class="row">
					<div class="col-md-12">
						<h3>${cliente.apellido}, ${cliente.nombre}</h3>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-5">
						
						<div class="row">
							<div class="col-md-12">
								
							<img src="http://via.placeholder.com/170x170">
							</div>
						</div>
						<br>
						<div class="row">
							<div class="col-md-12">
								<button class="btn btn-sm btn-success" style="width:170px;">Editar</button>
							</div>
							
						</div>
						

					</div>
					<div class="col-md-5 col-sm-7">
					
						<div class="row">
							<div class="col-md-6">Nombre:</div>
							<div class="col-md-6">${cliente.nombre}</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">Apellido:</div>
							<div class="col-md-6">${cliente.apellido}</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">Email</div>
							<div class="col-md-6">${cliente.email}</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">Fecha Nacimiento:</div>
							<div class="col-md-6">${cliente.fechaNacimiento}</div>
						</div>
						
						<div class="row">
							<div class="col-md-6">Ultima Modificacion:</div>
							<div class="col-md-6">${cliente.ultimaModificacion}</div>
						</div>
						
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>