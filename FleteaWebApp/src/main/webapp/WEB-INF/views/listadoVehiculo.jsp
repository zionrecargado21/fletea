<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-12 text-left">
						<div class="form-group">
							<label class="control-label col-sm-2" for="fletero">Fletero:</label>
							<div class="col-sm-10">
								<p class="form-control-static">
									<i class="fa fa-user-circle" aria-hidden="true"></i>
									${fletero.nombre}
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Mis Vehiculos Registrados</h3>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">
						<form action="registrarVehiculo" method="post">
							<div class="row mb-5">
								<div class="col-md-12">
									<table id="table"  data-pagination="true" data-page-size="5" data-search="true"
									data-toggle="table"  data-mobile-responsive="true"
                        mobileResponsive="true">
										<thead>
											<tr>
												<th data-sortable="true" data-field="id">ID</th>
												<th data-sortable="true" data-field="tipoDeVehiculo">Tipo de Vehiculo</th>
												<th data-sortable="true" data-field="patente">Patente</th>
												<th data-sortable="true" data-field="modelo">Modelo</th>
												<th data-sortable="true" data-field="a�o">A�o</th>
												<th data-sortable="true" data-field="capacidadDeCarga">Capacidad de Carga</th>
												<th>Tarifa por Hora</th>
												<th>Editar</th>
												<th>Borrar</th>
											</tr>
										</thead>
										<tbody>
											<c:forEach items="${listadoVehiculo}" var="tvehiculo"
												varStatus="loopCounter">

												<tr>
													<td>${tvehiculo.idVehiculo}</td>
													<td>${tvehiculo.tipoVehiculo.descripcion}</td>
													<td>${tvehiculo.patente}</td>
													<td>${tvehiculo.modeloVehiculo.descripcion}</td>
													<td>${tvehiculo.a�o}</td>
													<td>${tvehiculo.capacidadCarga}</td>
													<td>${tvehiculo.tarifaPorHora}</td>
													<td><a
														href="registrarVehiculo?idVehiculo=${tvehiculo.idVehiculo}">Editar
													</a></td>
													<td><a
														href="borrarVehiculo?idVehiculo=${tvehiculo.idVehiculo}">Borrar
													</a></td>
												</tr>

											</c:forEach>
										</tbody>

									</table>

								</div>
							</div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<h4>
											<a href="registrarVehiculo" class="btn btn-primary">
												Registrar nuevo</a>
										</h4>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>