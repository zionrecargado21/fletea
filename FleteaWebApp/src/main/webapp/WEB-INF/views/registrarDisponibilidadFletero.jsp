<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
			<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Registrar Disponibilidad Fletero</h3>
					</div>
				</div>
				<br>
				
					<form:form action="guardarDisponibilidadFletero" method="post"
					modelAttribute="disponibilidadFletero">
					
					
								<div class="row">
							
									<div class="col-md-4">										
										<form:hidden path="fletero.idPersona"   />
										<label for="idDia">D�a</label> 									 																
										<form:select path="dia.idDia" 
													class="form-control" 
													items="${allDias}" 
													itemValue="idDia" 
													itemLabel="nombre" /> 
									
								
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="horaDesde">Hora desde</label> 
											<form:input path="horaDesde" min="00" max="23" type="number" name="horaDesde" class="form-control"/>
										</div>
									</div>
									
									
									<div class="col-md-4">
										<div class="form-group">
											<label for="horaHasta">Hora hasta</label> 
											<form:input path="horaHasta" min="00" max="23" type="number" name="horaHasta" class="form-control"/>
										</div>
									</div>
								</div>
								<br>
								<div class="row">
									<div class="col-md-12" align=center>
									   <button type="submit" class="btn btn-primary">Registrar</button>
									</div>								
								</div>
								
					
					</form:form>
					
				<br>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										
									</div>
								</div>
								<div class="col-md-3"></div>
							</div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<h4>										
											<a href="listadoDisponibilidadFletero" class="btn btn-info"> Volver al listado</a>
										</h4>	
									</div>
								</div>
							</div>				
				</form>
			</div>
			</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>