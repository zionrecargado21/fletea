<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
						<h2>Ingreso de Usuarios</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<form name='f' action='j_spring_security_check' method='POST'>
							<div class="form-group">
								<label for="j_username">Usuario</label>								
								<input autofocus type="text" class="form-control" name="j_username"									
									placeholder="Ingrese su usuario"> 
							</div>
							<div class="form-group">
								<label for="j_password">Contraseņa</label> 
								<input
									type="password" class="form-control" name="j_password"
									placeholder="Ingrese su contraseņa">
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-primary">Ingresar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</header>
		
		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>

