<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Registrar Marca Veh�culo</h3>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-12">

						<form:form action="guardarMarcaVehiculo" method="post"
							modelAttribute="marca">

							<form:hidden path="idMarca" />

							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="marcaVehiculo">Marca de Veh�culo</label> 
											<form:input path="descripcion" name="descripcion" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<button type="submit" class="btn btn-primary">Registrar</button>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<h4>										
											<a href="listadoMarcaVehiculo" class="btn btn-info"> Volver al listado</a>
										</h4>	
									</div>
								</div>
							</div>							

						</form:form>

					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>