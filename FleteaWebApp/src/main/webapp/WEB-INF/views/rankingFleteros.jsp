<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
			
				<div class="row" style="padding-top: 10px; padding-bottom: 20px">
					<div class="col-md-4"></div>
					<div class="col-md-4" align=center>
						<h2>Ranking Fleteros</h2>
						<div class="form-group">
							<label>Top</label>
							<input autofocus="true" id="txtTopCant" type="number" class="form-control" />
						</div>					
						<button type="button" id="btnRankingFleteros" class="btn btn-primary">Cargar</button>
					</div>					
				</div>
				
				
				<div class="row pb-5">
					<div class="col-md-4"></div>
					<div class="col-md-4 text-center">
						
					<div class="table-responsive">
						<table id="tablaRankingFleteros" class="table table-hover">					     
							<thead>			
							  <tr>
							     <td>Cantidad Servicios</td>			
							     <td>Fletero</td>	
							     <td>Localidad</td>	
							     <td>Provincia</td>					  	
							  </tr>				
							</thead>
								<tbody>
									
								</tbody>
						</table>
						</div>
						
						<div id="grafico"></div>
					</div>
				</div>
			</div>
		</header>
		
		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
		
		


	</tiles:putAttribute>
</tiles:insertDefinition>

