<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-12 text-center">
						<h3>Registraci�n Cliente</h3>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">

						<form:form action="guardarCliente" method="post"
							modelAttribute="cliente">
							<form:hidden path="idPersona" />
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="nombre">Nombre</label>
										<form:input type="text" name="nombre" path="nombre"
											class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="calle">Calle</label>
										<form:input type="text" name="calle" path="calle"
											class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="numeroCalle">N�mero Calle</label>
										<form:input type="number" name="numeroCalle"
											path="numeroCalle" class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="numeroPiso">N�mero Piso</label>
										<form:input type="number" name="numeroPiso" path="numeroPiso"
											class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="form-group col-lg-6">
									<label for="provincia">Provincia</label> <select id="provincia"
										class="form-control" required="true">
										<option value="">Seleccione una Provincia</option>
										<c:choose>
											<c:when test="${provincias != null}">
												<c:forEach items="${provincias}" var="provincia">
													<c:choose>
														<c:when
															test="${provincia.idProvincia==cliente.barrio.localidad.provincia.idProvincia}">
															<option value="${provincia.idProvincia}" selected>${provincia.nombre}</option>
														</c:when>
														<c:otherwise>
															<option value="${provincia.idProvincia}">${provincia.nombre}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
								</div>
								<div class="form-group col-lg-6">
									<label for="localidad">Localidad</label> <select id="localidad"
										class="form-control" name="localidad">
										<option value=""></option>
										<c:choose>
											<c:when test="${localidades != null}">
												<c:forEach items="${localidades}" var="localidad">
													<c:choose>
														<c:when
															test="${localidad.idLocalidad==cliente.barrio.localidad.idLocalidad}">
															<option value="${localidad.idLocalidad}" selected>${localidad.nombre}</option>
														</c:when>
														<c:otherwise>
															<option value="${localidad.idLocalidad}">${localidad.nombre}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</select>
								</div>

								<div class="form-group col-lg-6">
									<label for="barrio">Barrio</label>
									<form:select path="barrio" id="barrio" class="form-control"
										name="barrio">
										<form:option value=""></form:option>
										<c:choose>
											<c:when test="${barrios != null}">
												<c:forEach items="${barrios}" var="barrio">
													<c:choose>
														<c:when test="${barrio.idBarrio==cliente.barrio.idBarrio}">
															<option value="${barrio.idBarrio}" selected>${barrio.nombre}</option>
														</c:when>
														<c:otherwise>
															<option value="${barrio.idBarrio}">${barrio.nombre}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:when>
										</c:choose>
									</form:select>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="telefono">N�mero telefono</label>
										<form:input type="tel" name="telefono" path="telefono"
											class="form-control" />
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label for="dni">DNI</label>
										<form:input type="number" name="dni" path="dni"
											class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="email">Email</label>
										<form:input type="email" name="mailUsuario" path="mailUsuario"
											class="form-control" />
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label for="password">Password</label>
										<form:input type="password" name="password" path="password"
											class="form-control" />
									</div>
								</div>
							</div>



							<!-- Disponibilidad de cliente -->
							<%--
							<br> <br>
							<div class="row">
								<div class="col-md-12">
									<div>
										<h3>Disponibilidad Horaria</h3>
									</div>
									<div>
										<table class="table table-inverse">
											<thead>
												<tr>
													<th>#</th>
													<th>D�a</th>
													<th>Hora Desde</th>
													<th>Hora Hasta</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<th scope="row">1</th>
													<td>Lunes</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
												<tr>
													<th scope="row">2</th>
													<td>Martes</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
												<tr>
													<th scope="row">3</th>
													<td>Miercoles</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
												<tr>
													<th scope="row">4</th>
													<td>Jueves</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
												<tr>
													<th scope="row">5</th>
													<td>Viernes</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
												<tr>
													<th scope="row">6</th>
													<td>Sabado</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
												<tr>
													<th scope="row">7</th>
													<td>Domingo</td>
													<td><input type="text" name="desde"
														class="form-control timepicker" size="1"></td>
													<td><input type="text" name="hasta"
														class="form-control timepicker" size="1"></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
 						--%>
							<div class="row">
								<div class="col-md-3"></div>
								<div class="col-md-6 text-center">
									<div class="form-group">
										<button type="submit" class="btn btn-primary">Registrar</button>
									</div>
								</div>
							</div>
						</form:form>
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>