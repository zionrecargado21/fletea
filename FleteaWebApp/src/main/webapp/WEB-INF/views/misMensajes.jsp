<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>


<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<link href="<c:url value="static/css/estilosMensajes.css" />"
			rel="stylesheet">
		<link
			href="<c:url value="static/lib/fontAwesome470/css/font-awesome.min.css" />"
			rel="stylesheet">

		<header class="masthead">
			<div class="container">
				<br>
				<div class="row">
					<div class="col-md-12">
						<!-- Inicio  -->
						<div class="row menu">
							<div class="col-md-3"></div>
							<div class="col-md-6">
								<div class="contenedorChat form-group">
									<div class="col-md-12 text-center tituloChat">
										<h3>Historial de Mensajes</h3>
									</div>
									<ol class="chat">
										<c:choose>
											<c:when test="${fn:length(listaMensajes) > 0 }">
												<c:forEach items="${listaMensajes}" var="mensajes"
													varStatus="loopCounter">
													<fmt:formatDate var="newFormattedDateString"
																value="${mensajes.fecha}" pattern="HH:mm dd-MM-yyyy" />
													<c:choose>
														<c:when
															test="${usuario.idPersona==mensajes.personaDestinatario.idPersona}">
															
															<li class="other">
																																												<div class="avatar"> 														<img src="https://i.imgur.com/DY6gND0.png"
																																														draggable="false" /> 													</div>
																<div class="msg">
																	<p>${mensajes.personaRemitente.nombre}</p>
																	<p>${mensajes.texto}</p>
																	<time>${newFormattedDateString}</time>
																</div>
															</li>
														</c:when>
														<c:otherwise>
															<li class="self">
																																												<div class="avatar"> 														<img src="https://i.imgur.com/DY6gND0.png"
																																														draggable="false" /> 													</div>
																<div class="msg">
																<p>${mensajes.personaRemitente.nombre}</p>
																	<p>${mensajes.texto}</p>
																	<time>${newFormattedDateString}</time>
																</div>
															</li>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</c:when>
											<c:otherwise>
												<div class="text-center" style="padding: 20px;">

													<p class="btn btn-danger">
														<i class="fa fa-envelope-open-o" aria-hidden="true"></i>
														No se encontraron mensajes.
													</p>

												</div>
											</c:otherwise>
										</c:choose>


									</ol>
								</div>
							</div>
						</div>

						<!-- Fin -->


						<div class="row">
							<div class="col-md-3"></div>
							<div class="col-md-6 text-center">

								<form name="enviarMensaje" action="guardarMensaje"
									class="formMensaje" method="POST">
									<div class="form-group">
										<div class="card">
											<div class="card-header">
												<i class="fa fa-paper-plane-o" aria-hidden="true"></i> ENVIO
												UN NUEVO MENSAJE
											</div>
											<div class="card-block">
												<div class="form-group">
													<textarea name="texto" rows="5" cols="40"
														class="form-control" required></textarea>
												</div>
												<input type="hidden" name="idPersonaRemitente"
													value="${usuario.idPersona}">
												
												<input type="hidden" name="idLector" value="${usuario.idPersona}"/>
												<input type="hidden" name="idServicio"
													value="${servicio.idServicio}"> <input
													type="submit" value="Enviar Mensaje"
													class="btn btn-primary" />
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>