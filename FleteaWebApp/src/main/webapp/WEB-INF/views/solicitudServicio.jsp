<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<tiles:insertDefinition name="defaultTemplate">
	<tiles:putAttribute name="body">
		<header class="masthead">
			<div class="container">
				<div class="row">
					<div class="col-lg-3"></div>
					<div class="col-lg-6">
						<div class="row">
							<div class="row">
								<div class="col-sm-12 text-center mt-5 mb-3">
									<h1>Solicitar servicio</h1>
								</div>
									<div class="card col-lg-6 mb-3" style="color:black;">
									  <div class="card-body">
									    <h4 class="card-title"><i class="fa fa-user-circle" aria-hidden="true"></i>
									    Cliente</h4>
									    <p class="card-text">
									    	
											<div class="form-group col-lg-12 text-center">
												<label>Nombre: ${servicio1.cliente.nombre}</label>
											</div>
									    </p>
									  </div>
									</div>
									<div class="card col-lg-6 mb-3" style="color:black;">
									  <div class="card-body">
									    <h4 class="card-title"><i class="fa fa-user-circle-o" aria-hidden="true"></i>
									    Fletero</h4>
									    <p class="card-text">
									    	
											<div class="form-group col-lg-12 text-center">
												<label>Nombre / Razon Social: ${servicio1.fletero.nombre}</label>
											</div>
									    </p>
									  </div>
									</div>
								<div class="col-md-12 card mb-5" style="width:100%;color:black;">
								  <div class="card-header">
								   <i class="fa fa-truck" aria-hidden="true"></i>
								    Veh�culo
								  </div>
								  <ul class="list-group list-group-flush">
								    <li class="list-group-item">Tipo vehiculo: ${servicio1.vehiculo.tipoVehiculo.descripcion}</li>
								    <li class="list-group-item">Tipo vehiculo: ${servicio1.vehiculo.tipoVehiculo.descripcion}</li>
								    <li class="list-group-item">Patente: ${servicio1.vehiculo.patente}</li>
								    <li class="list-group-item">Capacidad carga: ${servicio1.vehiculo.capacidadCarga}</li>
								    <li class="list-group-item">Tarifa/Hora: ${servicio1.vehiculo.tarifaPorHora}</li>
								  </ul>
								</div>
								<h2 class="col-sm-12 text-center">Datos de entrega</h2>
								
								<form:form name="enviarServicio" action="guardarServicio"
										class="row" method="POST" modelAttribute="servicio">
										
									<div class="form-group col-lg-6">
										<label for="provincia">Provincia</label>
										<select id="provincia" class="form-control" required="true">
											<option value="">Seleccione una Provincia</option>
										<c:forEach items="${provincias}" var="provincia">
											<option value="${provincia.idProvincia}">${provincia.nombre}</option>
										</c:forEach>
										</select>
									</div>
									<div class="form-group col-lg-6">
										<label for="localidad">Localidad</label>
										<select id="localidad" class="form-control" name="localidad">
											<option value=""></option>
										</select>
									</div>
									
									<div class="form-group col-lg-12">
										<label for="barrio">Barrio</label>
										<form:select  path="barrio"  id="barrio" class="form-control" name="barrio">
										 	<form:option value=""></form:option>
										</form:select>
									</div>
									
									<div class="form-group col-lg-6">
										<label for="fecha">Fecha</label> <form:input path="fechaSolicitud" type="date"
											class="form-control" id="fecha" aria-describedby="emailHelp" required="true"
											placeholder="Ingrese la fecha" name="fechaSolicitud"/> 
									</div>
									
									<div class="form-group col-lg-6">
										<label for="hora">Hora</label> <input type="text" 
											class="form-control timepicker" id="hora" aria-describedby="emailHelp"
											placeholder="Ingrese la hora" path="hora" name="hora">
									</div>
									<div class="form-group col-lg-6">
										<label for="calle">Calle</label> <form:input type="text"
	 										class="form-control" id="calle" path="calle" 
	 										aria-describedby="emailHelp"
	 										placeholder="Ingrese la calle"/> 
									</div>
									<div class="form-group col-lg-6">
										<label for="piso">Piso</label> <form:input type="text"
	 										class="form-control" id="piso" path="numeroPiso" 
	 										aria-describedby="emailHelp" 
	 										placeholder="Ingrese el piso"/> 
									</div>
									<div class="form-group col-lg-6">
										<label for="numeroCalle">N�mero de Calle</label> <form:input type="text"
	 										class="form-control" id="numeroCalle" path="numeroCalle" 
	 										aria-describedby="emailHelp" 
	 										placeholder="Ingrese el n�mero"/> 
									</div>
									<div class="form-group col-lg-6">
										<label for="dpto">Departamento</label> <form:input type="text"
	 										class="form-control" id="dpto" path="dpto" 
	 										aria-describedby="emailHelp" 
	 										placeholder="Ingrese el departamento"/> 
									</div>
									<div class="form-group col-lg-12">
										<label for="dpto">Aclaraciones</label> <form:textarea type="text"
	 										class="form-control" id="descripcion" path="descripcion" 
	 										aria-describedby="emailHelp" 
	 										placeholder="Ingrese alguna aclaraci�n que desee hacer" rows="7"></form:textarea> 
									</div>
									
									<div class="col-sm-12 text-center mt-4 mb-5">
										<button type="submit" class="btn btn-primary">Solicitar Servicio</button>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>

		<section class="contact bg-primary" id="contact">
			<div class="container">
				<h2>
					Nos <i class="fa fa-heart"></i> nuevos amigos! Sumate!
				</h2>
				<ul class="list-inline list-social">
					<li class="list-inline-item social-twitter"><a href="#"> <i
							class="fa fa-twitter"></i>
					</a></li>
					<li class="list-inline-item social-facebook"><a href="#">
							<i class="fa fa-facebook"></i>
					</a></li>
					<li class="list-inline-item social-google-plus"><a href="#">
							<i class="fa fa-google-plus"></i>
					</a></li>
				</ul>
			</div>
		</section>
	</tiles:putAttribute>
</tiles:insertDefinition>