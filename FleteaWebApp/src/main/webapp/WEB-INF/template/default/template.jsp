<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ include file="include.jsp"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>Flete�!</title>

<!-- Bootstrap core CSS -->
<link
	href="<c:url value="/static/vendor/bootstrap/css/bootstrap.min.css"/>"
	rel="stylesheet" />

<!-- Custom fonts for this template -->
<link
	href="<c:url value="/static/vendor/font-awesome/css/font-awesome.min.css"/>"
	rel="stylesheet" />

<link
	href="<c:url value="/static/vendor/simple-line-icons/css/simple-line-icons.css"/>"
	rel="stylesheet" />

<link href="https://fonts.googleapis.com/css?family=Lato"
	rel="stylesheet">

<link
	href="https://fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Muli"
	rel="stylesheet">

<!-- Plugin CSS -->
<link
	href="<c:url value="/static/device-mockups/device-mockups.min.css"/>"
	rel="stylesheet" />

<!-- Custom styles for this template -->
<link href="<c:url value="/static/css/new-age-site.css"/>"
	rel="stylesheet" />

<!-- TimerPicker  -->
<link
	href="<c:url value="/static/lib/jquery-timepicker/jquery.timepicker.min.css"/>"
	rel="stylesheet" />

<!-- Bootstrap table -->

<link rel="stylesheet"
	href="<c:url value="/static/lib/bootstrap-table/bootstrap-table.css"/>" />


<!-- Morris -->
<!--   <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css"> -->
  <link rel="stylesheet" href="<c:url value="/static/lib/morris/morris.css" />">
  
  
  
</head>
<body id="page-top">
	<div class="page">
		<tiles:insertAttribute name="header" />
		<tiles:insertAttribute name="body" />
		<tiles:insertAttribute name="footer" />
	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="<c:url value="/static/vendor/jquery/jquery.min.js" />"
		type="text/javascript"></script>
	<script src="<c:url value="/static/vendor/popper/popper.min.js" />"
		type="text/javascript"></script>
	<script
		src="<c:url value="/static/vendor/bootstrap/js/bootstrap.min.js" />"
		type="text/javascript"></script>

	<!-- Plugin JavaScript -->
	<script
		src="<c:url value="/static/vendor/jquery-easing/jquery.easing.min.js" />"
		type="text/javascript"></script>

	<!-- Custom scripts for this template -->
	<script src="<c:url value="/static/js/new-age.min.js" />"
		type="text/javascript"></script>

	<!-- Timepicker JS -->
	<script
		src="<c:url value="/static/lib/jquery-timepicker/jquery.timepicker.min.js" />"
		type="text/javascript"></script>

	<!-- Util JS -->
	<script src="<c:url value="/static/js/util.js" />"
		type="text/javascript"></script>
		
 	<!--  Morris and Raphael -->
 	<script src="<c:url value="/static/lib/morris/raphael-min.js" />"></script>
 	<script src="<c:url value="/static/lib/morris/morris.min.js" />"></script>
<!-- 	<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script> -->
<!-- 	<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script> -->
  
  
	<!-- Ajax JS -->
	<script src="<c:url value="/static/js/ajax.js" />"
		type="text/javascript"></script>

	<!-- Bootstrap table  -->
	<script src="<c:url value="/static/lib/bootstrap-table/bootstrap-table.js" />"
		type="text/javascript"></script>
	
	<!-- Bootstrap language  -->	
	<script src="<c:url value="/static/lib/bootstrap-table/locale/bootstrap-table-es-AR.js" />"
		type="text/javascript"></script>
		
</body>
</html>