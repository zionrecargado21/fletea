<footer>
      <div class="container">
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Rodrigo Garcia</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Franco Funes</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Lolli Gustavo</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Agustina Ziegler</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Matias Sosa</a>
          </li>
        </ul>
      </div>
    </footer>