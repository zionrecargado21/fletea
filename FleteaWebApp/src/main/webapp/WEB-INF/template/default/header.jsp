<%@page import="com.zion.fletea.domain.UsuarioLogueado"%>
<%@ include file="include.jsp"%>


<c:set var="contextPath" value="${pageContext.request.contextPath}" />
<nav class="navbar navbar-expand-lg navbar-light" id="mainNav">
	<div class="container">
		<a class="navbar-brand js-scroll-trigger" href="${contextPath}/index">Flete�!</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			Menu <i class="fa fa-bars"></i>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">

				<%
				if (request.getRemoteUser()== null) {%>  				    
				    
				    <li class="nav-item"><a class="nav-link" href="home">Inicio</a></li>
				    <li class="nav-item"><a class="nav-link js-scroll-trigger" href="#contact">Contactanos</a></li>
				    <li class="nav-item"><a class="nav-link" href="registrarse">Registrarme</a></li>
				    <li class="nav-item"><a class="nav-link" href="login">Ingresar</a></li>
				    
				<%}else if(UsuarioLogueado.getRol(request).equals("Fletero")){%>					

					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/bienvenido">Home</a></li>
						
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/registrarVehiculo">Registrar Veh�culo</a></li>		
						
						
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/listadoDisponibilidadFletero">Disponibilidad Fletero</a></li>			
														
					<li class="nav-item"><a class="nav-link" href="logout">Salir</a></li>
					
				<%}else if(UsuarioLogueado.getRol(request).equals("Cliente")){%>
					
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/bienvenido">Home</a></li>												
					
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/busquedaServiciosDisponibles">Busqueda Servicios</a></li>	
										
					<li class="nav-item"><a class="nav-link" href="logout">Salir</a></li>
					
				<%}else if(UsuarioLogueado.getRol(request).equals("Administrador")){%>
								
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/bienvenido">Home</a></li>
					
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/listadoTipoVehiculo">Tipo de Veh�culo</a></li>
	
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/listadoModelo">Modelo de Veh�culo</a></li>
					
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/listadoMarcaVehiculo">Marca de Veh�culo</a></li>
		
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/listadoFletero">Fletero</a></li>

					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/ServiciosConcretados">Servicios por Periodo</a></li>
						
					<li class="nav-item"><a class="nav-link"
						href="${contextPath}/RankingFleteros">Ranking Fleteros</a></li>
					
					<li class="nav-item"><a class="nav-link" href="logout">Salir</a></li>
					
				<%}%>

			</ul>
		</div>
	</div>
</nav>