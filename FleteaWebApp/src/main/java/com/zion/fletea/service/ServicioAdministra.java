package com.zion.fletea.service;


import java.util.List;

import com.zion.fletea.domain.Servicio;



public interface ServicioAdministra {
	
	public List<Servicio> getServicios();
	
	public void guardarServicio(Servicio servicio);
	
	public void borraServicio(int idServicio);
	
	public void updateServicio(Servicio Servicio);
	
	public Servicio getServicio(int idServicio);
	
	public List<Servicio> getServicioByCliente(int idPersona);
	
	public List<Servicio> getServicioByFletero(int idPersona);
	
}