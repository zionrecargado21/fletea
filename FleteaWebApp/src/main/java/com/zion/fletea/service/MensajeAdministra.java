package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.Mensaje;

public interface MensajeAdministra {

	public List<Mensaje> getMensajes(int idServicio);
	
	public void guardarMensaje(Mensaje mensaje);
	
}