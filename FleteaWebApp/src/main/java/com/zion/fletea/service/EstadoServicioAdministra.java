package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.EstadoServicio;

public interface EstadoServicioAdministra {

	public List<EstadoServicio> getEstadoServicios();
	
	public void guardarEstadoServicio(EstadoServicio estadoServicio);
	
	public void borrarEstadoServicio(Integer idEstadoServicio);
	
	public void actualizarEstadoServicio(EstadoServicio estadoServicio);
	
	public EstadoServicio getEstadoServicio(int idEstadoServicio);
	
	
}
