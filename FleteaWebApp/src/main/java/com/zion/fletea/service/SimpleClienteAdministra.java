package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Cliente;
import com.zion.fletea.repository.ClienteDAO;

import java.util.List;

@Component
public class SimpleClienteAdministra implements ClienteAdministra {

	@Autowired
	private ClienteDAO clienteDAO;

	public List<Cliente> getClientes() {
		return clienteDAO.getClienteList();
	}

	@Override
	public void guardarCliente(Cliente cliente) {
		clienteDAO.guardarCliente(cliente);
	}


	@Override
	public void updateCliente(Cliente cliente) {
		clienteDAO.actualizarCliente(cliente);
	}

	@Override
	public Cliente getCliente(int idCliente) {
		return clienteDAO.getCliente(idCliente);
	}

	@Override
	public void borraCliente(Integer idCliente) {
		// TODO Auto-generated method stub
		
	}

}
