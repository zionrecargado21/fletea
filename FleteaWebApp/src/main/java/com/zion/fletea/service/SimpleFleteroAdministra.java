package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Fletero;
import com.zion.fletea.repository.BarrioDAO;
import com.zion.fletea.repository.FleteroDAO;

import java.util.List;

import javax.persistence.Query;

@Component
public class SimpleFleteroAdministra implements FleteroAdministra{

	@Autowired
	private FleteroDAO fleteroDAO;
	
	@Autowired
	private BarrioDAO barrioDAO;
	
	@Override
	public List<Fletero> getFleteros() {
		return fleteroDAO.getAllFletero();
	}

	@Override
	public void guardarFletero(Fletero fletero) {
		fleteroDAO.guardarFletero(fletero);	
	}

	@Override
	public void updateFletero(Fletero fletero) {
		fleteroDAO.updateFletero(fletero);
	}

	@Override
	public Fletero getFletero(int idFletero) {
		return fleteroDAO.getFletero(idFletero);
	}

	@Override
	public void borraFletero(Integer idFletero) {
		fleteroDAO.borrarFletero(idFletero);
		
	}

	public List<Barrio> getAllBarrios() {
		return barrioDAO.getAllBarrio();
		//return null;
	}

	@Override
	public List<Fletero> getFleteroPorNombre(String nombreParcial) {
		return fleteroDAO.getFleteroByName(nombreParcial);
	}

	

	public List<Integer> getFleteroPorNombreYBarrio(String nombreFletero, Barrio barrio) {
		return fleteroDAO.getFleteroPorNombreYBarrio(nombreFletero, barrio);
	}

}
