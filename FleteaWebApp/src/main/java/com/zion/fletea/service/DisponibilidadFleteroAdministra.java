package com.zion.fletea.service;

import java.util.List;

import com.zion.fletea.domain.Dia;
import com.zion.fletea.domain.DisponibilidadFletero;


public interface DisponibilidadFleteroAdministra {
	
	public List<Dia> getAllDias();
	
	public List<DisponibilidadFletero> getDisponibilidadFleteros();
	
	public void guardarDisponibilidadFletero(DisponibilidadFletero disponibilidadFleterofletero);
	
	public void borraDisponibilidadFletero(Integer idPersona, Integer idDia, Integer horaDesde, Integer horaHasta);

	public DisponibilidadFletero getDisponibilidadFletero(Integer idPersona, Integer idDia, Integer horaDesde, Integer horaHasta);

	public List<DisponibilidadFletero> getDisponibilidadPorFletero(int idFletero);

	
	
}