package com.zion.fletea.service;

import java.util.List;

import com.zion.fletea.domain.Barrio;



public interface BarrioAdministra {
	public List<Barrio> getBarrio();
	
	public void guardarBarrio(Barrio barrio);
	
	public void borraBarrio(Integer idBarrio);
	
	public void updateBarrio(Barrio barrio);
	
	public Barrio getBarrio(int idBarrio);

	public List<Barrio> getAllBarrioByLocalidad(int idLocalidad);
	
}
