package com.zion.fletea.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Localidad;
import com.zion.fletea.repository.LocalidadDAO;

@Component
public class SimpleLocalidadAdministra implements LocalidadAdministra{

	@Autowired
	private LocalidadDAO localidadDAO;

	@Override
	public List<Localidad> getAllLocalidad() {
		
		return localidadDAO.getAllLocalidad();
	}

	@Override
	public List<Localidad> getAllLocalidadByProvincia(int idProvincia) {
		
		return localidadDAO.getAllLocalidadByProvincia(idProvincia);
		
	}
	
	@Override
	public List<Localidad> getLocalidades() {
		return localidadDAO.getAllLocalidad();
	}

	@Override
	public void guardarLocalidad(Localidad localidad) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borrarLocalidad(Integer idLocalidad) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateLocalidad(Localidad localidad) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Localidad getLocalidad(int idLocalidad) {
		// TODO Auto-generated method stub
		return null;
	}

}
