package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.repository.MarcaDAO;

import java.util.List;

@Component
public class SimpleMarcaAdministra implements MarcaAdministra {

	@Autowired
	private MarcaDAO marcaDAO;

	public List<Marca> getMarcas() {
		return marcaDAO.getAllMarca();
	}

	@Override
	public void guardarMarca(Marca marca) {
		marcaDAO.guardarMarca(marca);
	}


	public void borraMarca(Integer idMarca) {
		marcaDAO.borrarMarca(idMarca);
		
	}

	@Override
	public void updateMarca(Marca marca) {
		marcaDAO.updateMarca(marca);
	}

	@Override
	public Marca getMarca(int idMarca) {
		return marcaDAO.getMarca(idMarca);
	}

}
