package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.Marca;

public interface MarcaAdministra {

	public List<Marca> getMarcas();
	
	public void guardarMarca(Marca marca);
	
	public void borraMarca(Integer idMarca);
	
	public void updateMarca(Marca marca);
	
	public Marca getMarca(int idMarca);
	
	
}
