package com.zion.fletea.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.repository.BarrioDAO;

@Component
public class SimpleBarrioAdministra implements BarrioAdministra{

	@Autowired
	private BarrioDAO barrioDAO;
	
	@Override
	public List<Barrio> getBarrio() {
		return barrioDAO.getAllBarrio();
	}

	@Override
	public void guardarBarrio(Barrio barrio) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void borraBarrio(Integer idBarrio) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateBarrio(Barrio barrio) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Barrio getBarrio(int idBarrio) {
		return barrioDAO.getBarrio(idBarrio);
	}

	@Override
	public List<Barrio> getAllBarrioByLocalidad(int idLocalidad) {
		
		return barrioDAO.getAllBarrioByLocalidad(idLocalidad);
		
	}

}
