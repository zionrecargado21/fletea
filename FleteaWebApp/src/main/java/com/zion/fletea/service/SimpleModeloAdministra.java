package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;
import com.zion.fletea.repository.MarcaDAO;
import com.zion.fletea.repository.ModeloDAO;

import java.util.List;

@Component
public class SimpleModeloAdministra implements ModeloAdministra {

	@Autowired
	private ModeloDAO modeloDAO;
	
	@Autowired
	private MarcaDAO marcaDAO;

	@Override
	public void guardarModelo(Modelo modelo) {

		modeloDAO.guardarModelo(modelo);

	}

	@Override
	public void updateModelo(Modelo modelo) {

		modeloDAO.updateModelo(modelo);

	}

	@Override
	public Modelo getModelo(int idModelo) {
		return modeloDAO.getModelo(idModelo);
	}

	@Override
	public List<Modelo> getModelos() {
		return modeloDAO.getAllModelo();
	}

	@Override
	public void borraModelo(Integer idModelo) {
		modeloDAO.borrarModelo(idModelo);

	}

	public List<Marca> getAllMarcas() {
		return marcaDAO.getAllMarca();
	}

}
