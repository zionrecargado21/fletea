package com.zion.fletea.service;

import java.io.Serializable;
import java.util.List;
import com.zion.fletea.domain.Provincia;

public interface ProvinciaAdministra extends Serializable{

	public List<Provincia> getProvincias();
	
	
}
