package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Mensaje;
import com.zion.fletea.repository.MensajeDAO;

import java.util.List;

@Component
public class SimpleMensajeAdministra implements MensajeAdministra {

	@Autowired
	private MensajeDAO mensajeDAO;

	@Override
	public List<Mensaje> getMensajes(int idServicio) {
		return mensajeDAO.getAllMensajes(idServicio);
	}

	@Override
	public void guardarMensaje(Mensaje mensaje) {
		
		mensajeDAO.guardarMensaje(mensaje);
		
	}

}
