package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Dia;
import com.zion.fletea.domain.DisponibilidadFletero;
import com.zion.fletea.repository.DiaDAO;
import com.zion.fletea.repository.DisponibilidadFleteroDAO;

import java.util.List;

@Component
public class SimpleDisponibilidadFleteroAdministra implements DisponibilidadFleteroAdministra{

	@Autowired
	private DisponibilidadFleteroDAO DisponibilidadFleteroDAO;
	
	@Autowired
	private DiaDAO diaDAO;
	
	@Override
	public List<DisponibilidadFletero> getDisponibilidadFleteros() {
		return DisponibilidadFleteroDAO.getAllDisponibilidadFletero();   
	}

	@Override
	public void guardarDisponibilidadFletero(DisponibilidadFletero disponibilidadFletero) {
		DisponibilidadFleteroDAO.guardarDisponibilidadFletero(disponibilidadFletero);	
	}


	@Override
	public DisponibilidadFletero getDisponibilidadFletero(Integer idPersona, Integer idDia, Integer horaDesde, Integer horaHasta) {
		return DisponibilidadFleteroDAO.getDisponibilidadFletero(idPersona, idDia, horaDesde, horaHasta);
	}

	
	@Override
	public void borraDisponibilidadFletero(Integer idPersona, Integer idDia, Integer horaDesde, Integer horaHasta) {
		DisponibilidadFleteroDAO.borrarDisponibilidadFletero(idPersona, idDia, horaDesde, horaHasta);
		
	}

	public List<Dia> getAllDias() {
		return diaDAO.getAllDia();
		//return null;
	}

	@Override
	public List<DisponibilidadFletero> getDisponibilidadPorFletero(int idFletero) {
		return DisponibilidadFleteroDAO.getDisponibilidadPorFletero(idFletero);
	}

}

