package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.EstadoServicio;
import com.zion.fletea.domain.Marca;
import com.zion.fletea.repository.EstadoServicioDAO;
import com.zion.fletea.repository.MarcaDAO;

import java.util.List;

@Component
public class SimpleEstadoServicioAdministra implements EstadoServicioAdministra {

	@Autowired
	private EstadoServicioDAO estadoServicioDAO;

	
	public List<EstadoServicio> getEstadoServicios() {
		return estadoServicioDAO.getAllEstadoServicios();
	}
	
	public void guardarEstadoServicio(EstadoServicio estadoServicio) {
		estadoServicioDAO.guardarEstadoServicio(estadoServicio);
		
	}

	
	public void borrarEstadoServicio(Integer idEstadoServicio) {
		estadoServicioDAO.borrarEstadoServicio(idEstadoServicio);
		
	}
	
	@Override
	public void actualizarEstadoServicio(EstadoServicio estadoServicio) {
		estadoServicioDAO.actualizarEstadoServicio(estadoServicio);
		
	}

	@Override
	public EstadoServicio getEstadoServicio(int idEstadoServicio) {
		return estadoServicioDAO.getEstadoServicio(idEstadoServicio);
	}
	

	

	

	


}
