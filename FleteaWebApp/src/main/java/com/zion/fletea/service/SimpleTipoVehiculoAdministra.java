package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.repository.TipoVehiculoDAO;

import java.util.List;

@Component
public class SimpleTipoVehiculoAdministra implements TipoVehiculoAdministra{

	@Autowired
	private TipoVehiculoDAO tipovehiculoDAO;
	
	@Override
	public List<TipoVehiculo> getTipoVehiculos() {
		// TODO Auto-generated method stub
		return tipovehiculoDAO.getAllTipoVehiculo();

	}

	@Override
	public void guardarTipoVehiculo(TipoVehiculo tipovehiculo) {

		tipovehiculoDAO.guardarTipoVehiculo(tipovehiculo);
		
	}

	public void borraTipoVehiculo(Integer tipovehiculo) {
		
		tipovehiculoDAO.borrarTipoVehiculo(tipovehiculo);
		
	}

	@Override
	public void updateTipoVehiculo(TipoVehiculo tipovehiculo) {
		
		tipovehiculoDAO.updateTipoVehiculo(tipovehiculo);
		
	}

	@Override
	public TipoVehiculo getTipoVehiculo(int idTipoVehiculo) {
		return tipovehiculoDAO.getTipoVehiculo(idTipoVehiculo);
	}


}
