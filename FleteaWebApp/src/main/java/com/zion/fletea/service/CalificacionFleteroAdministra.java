package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.CalificacionFletero;

public interface CalificacionFleteroAdministra {

	public List<CalificacionFletero> getCalificacionFleteros();
	
	public void guardarCalificacionFletero(CalificacionFletero calificacionFletero);
	
	public void borraCalificacionFletero(Integer idCalificacionFletero);
	
	public void updateCalificacionFletero(CalificacionFletero calificacionFletero);
	
	public CalificacionFletero getCalificacionFletero(int idCalificacionFletero);
	
	public double getCalificacionPromedioFletero(int idFletero);
	
	public CalificacionFletero getCalificacionFleteroServicio(int idServicio);
}
