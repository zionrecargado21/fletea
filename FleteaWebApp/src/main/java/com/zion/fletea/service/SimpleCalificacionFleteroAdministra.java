package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.CalificacionFletero;
import com.zion.fletea.domain.Marca;
import com.zion.fletea.repository.CalificacionFleteroDAO;
import com.zion.fletea.repository.MarcaDAO;

import java.util.List;

@Component
public class SimpleCalificacionFleteroAdministra implements CalificacionFleteroAdministra {

	@Autowired
	private CalificacionFleteroDAO calificacionFleteroDAO;

	@Override
	public List<CalificacionFletero> getCalificacionFleteros() {
		return calificacionFleteroDAO.getAllCalificacionesFletero();
	}

	
	@Override
	public void guardarCalificacionFletero(CalificacionFletero calificacionFletero) {
		calificacionFleteroDAO.guardarCalificacionFletero(calificacionFletero);
		
	}
	
	@Override
	public void borraCalificacionFletero(Integer idCalificacionFletero) {
		calificacionFleteroDAO.borrarCalificacionFletero(idCalificacionFletero);
		
	}
	
	@Override
	public void updateCalificacionFletero(CalificacionFletero calificacionFletero) {
		calificacionFleteroDAO.updateCalificacionFletero(calificacionFletero);
		
	}
	
	@Override
	public CalificacionFletero getCalificacionFletero(int idCalificacionFletero) {
		return calificacionFleteroDAO.getCalificacionFletero(idCalificacionFletero);
	}


	@Override
	public double getCalificacionPromedioFletero(int idFletero) {
		return calificacionFleteroDAO.getCalificacionPromedioFletero(idFletero);
	}

	@Override
	public CalificacionFletero getCalificacionFleteroServicio(int idServicio) {
		return calificacionFleteroDAO.getCalificacionFleteroServicio(idServicio);
	}

}
