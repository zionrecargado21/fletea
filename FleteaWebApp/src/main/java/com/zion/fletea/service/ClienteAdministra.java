package com.zion.fletea.service;

import java.util.List;

import com.zion.fletea.domain.Cliente;

public interface ClienteAdministra {

	public List<Cliente> getClientes();

	public void guardarCliente(Cliente cliente);

	public void borraCliente(Integer idCliente);

	public void updateCliente(Cliente cliente);

	public Cliente getCliente(int idCliente);

}
