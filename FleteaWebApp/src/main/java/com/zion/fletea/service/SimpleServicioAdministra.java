package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Servicio;
import com.zion.fletea.repository.MarcaDAO;
import com.zion.fletea.repository.ServicioDAO;

import java.util.List;

@Component
public class SimpleServicioAdministra implements ServicioAdministra {

	@Autowired
	private ServicioDAO servicioDAO;	
	
	@Override
	public List<Servicio> getServicios() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void guardarServicio(Servicio servicio) {
		// TODO Auto-generated method stub
		servicioDAO.guardarServicio(servicio);
	}

	@Override
	public void borraServicio(int idServicio) {
		// TODO Auto-generated method stub
		servicioDAO.borrarServicio(servicioDAO.getServicio(idServicio));
	}

	@Override
	public void updateServicio(Servicio servicio) {
		// TODO Auto-generated method stub
		servicioDAO.updateServicio(servicio);
	}

	@Override
	public Servicio getServicio(int idServicio) {
		return servicioDAO.getServicio(idServicio);
	}

	@Override
	public List<Servicio> getServicioByCliente(int idPersona) {
		return servicioDAO.getServicioByCliente(idPersona);
	}

	@Override
	public List<Servicio> getServicioByFletero(int idPersona) {
		return servicioDAO.getServicioByFletero(idPersona);
	}


}
