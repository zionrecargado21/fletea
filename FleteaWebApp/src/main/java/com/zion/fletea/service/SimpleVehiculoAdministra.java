package com.zion.fletea.service;

import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.domain.Vehiculo;
import com.zion.fletea.repository.VehiculoDAO;

@Component
public class SimpleVehiculoAdministra implements VehiculoAdministra {
	
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private VehiculoDAO vehiculoDAO;

	public VehiculoDAO getVehiculoDAO() {
		return vehiculoDAO;
	}

	public void setVehiculoDAO(VehiculoDAO vehiculoDAO) {
		this.vehiculoDAO = vehiculoDAO;
	}

	@Override
	public List<Vehiculo> getVehiculosList() {
		return vehiculoDAO.getVehiculosList();
	}
	
	@Override
	public Vehiculo getVehiculo(int idVehiculo) {
		return vehiculoDAO.getVehiculo(idVehiculo);
	}

	@Override
	public void guardarVehiculo(Vehiculo vehiculo) {
		vehiculoDAO.actualizarVehiculo(vehiculo);
		
	}

	@Override
	public void actualizarVehiculo(Vehiculo vehiculo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Vehiculo> getVehiculosPorFleterosYTipoVehiculo(List<Integer> filtrado, TipoVehiculo tipoVehiculo) {
		return vehiculoDAO.getVehiculoPorFleterosYTipoVehiculo(filtrado, tipoVehiculo);
	}

	public void borrarVehiculo(int idVehiculo) {
		vehiculoDAO.borrarVehiculo(idVehiculo);
		
	}

	@Override
	public List<Vehiculo> getVehiculosPorFleteroList(Integer idPersona) {
		return vehiculoDAO.getVehiculosPorFleteroList(idPersona);
	}

}
