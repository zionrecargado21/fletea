package com.zion.fletea.service;

import java.util.List;

import javax.persistence.Query;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Fletero;


public interface FleteroAdministra {
	public List<Barrio> getAllBarrios();
	
	public List<Fletero> getFleteros();
	
	public void guardarFletero(Fletero fletero);
	
	public void borraFletero(Integer idFletero);
	
	public void updateFletero(Fletero fletero);
	
	public Fletero getFletero(int idFletero);
	
	public List<Fletero> getFleteroPorNombre(String nombreParcial);
	
	public List<Integer> getFleteroPorNombreYBarrio(String nombreParcial, Barrio barrio);
	
}
