package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.CalificacionCliente;

public interface CalificacionClienteAdministra {

	public List<CalificacionCliente> getCalificacionClientes();
	
	public void guardarCalificacionCliente(CalificacionCliente calificacionCliente);
	
	public void borraCalificacionCliente(Integer idCalificacionCliente);
	
	public void updateCalificacionCliente(CalificacionCliente calificacionCliente);
	
	public CalificacionCliente getCalificacionCliente(int idCalificacionCliente);
	
	public CalificacionCliente getCalificacionClienteServicio(int idServicio);
}
