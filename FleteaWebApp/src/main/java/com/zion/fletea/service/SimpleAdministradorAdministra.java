package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Administrador;
import com.zion.fletea.repository.AdministradorDAO;

@Component
public class SimpleAdministradorAdministra implements AdministradorAdministra {

	@Autowired
	private AdministradorDAO administradorDAO;


	@Override
	public Administrador getAdministrador(int idPersona) {
		return administradorDAO.getAdministrador(idPersona);
	}

}