package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.CalificacionCliente;
import com.zion.fletea.repository.CalificacionClienteDAO;

import java.util.List;

@Component
public class SimpleCalificacionClienteAdministra implements CalificacionClienteAdministra {

	@Autowired
	private CalificacionClienteDAO calificacionClienteDAO;

	@Override
	public List<CalificacionCliente> getCalificacionClientes() {
		return calificacionClienteDAO.getAllCalificacionesCliente();
	}

	
	@Override
	public void guardarCalificacionCliente(CalificacionCliente calificacionCliente) {
		calificacionClienteDAO.guardarCalificacionCliente(calificacionCliente);
		
	}
	
	@Override
	public void borraCalificacionCliente(Integer idCalificacionCliente) {
		calificacionClienteDAO.borrarCalificacionCliente(idCalificacionCliente);
		
	}
	
	@Override
	public void updateCalificacionCliente(CalificacionCliente calificacionCliente) {
		calificacionClienteDAO.updateCalificacionCliente(calificacionCliente);
		
	}
	
	@Override
	public CalificacionCliente getCalificacionCliente(int idCalificacionCliente) {
		return calificacionClienteDAO.getCalificacionCliente(idCalificacionCliente);
	}

	@Override
	public CalificacionCliente getCalificacionClienteServicio(int idServicio) {
		return calificacionClienteDAO.getCalificacionClienteServicio(idServicio);
	}

}
