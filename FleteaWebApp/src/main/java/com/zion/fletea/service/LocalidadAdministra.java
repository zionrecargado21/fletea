package com.zion.fletea.service;

import java.util.List;
import com.zion.fletea.domain.Localidad;



public interface LocalidadAdministra {
	
	public List<Localidad> getAllLocalidad();
	
	public List<Localidad> getAllLocalidadByProvincia(int idProvincia);

	public List<Localidad> getLocalidades();
	
	public void guardarLocalidad(Localidad localidad);
	
	public void borrarLocalidad(Integer idLocalidad);
	
	public void updateLocalidad(Localidad localidad);
	
	public Localidad getLocalidad(int idLocalidad);
	
}
