package com.zion.fletea.service;

import com.zion.fletea.domain.TrazabilidadServicio;

public interface ServicioTrazabilidadAdministra {
	public void guardarServicio(TrazabilidadServicio servicio);
	public TrazabilidadServicio getEstadoServicio(int idServicio);
}
