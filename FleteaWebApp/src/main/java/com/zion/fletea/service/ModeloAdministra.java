package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;

public interface ModeloAdministra {

	public List<Modelo> getModelos();
	
	public List<Marca> getAllMarcas();
	
	public void guardarModelo(Modelo modelo);
	
	public void borraModelo(Integer idModelo);
	
	public void updateModelo(Modelo modelo);
	
	public Modelo getModelo(int idModelo);
	
	
}
