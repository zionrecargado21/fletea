package com.zion.fletea.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Provincia;
import com.zion.fletea.repository.ProvinciaDAO;

@Component
public class SimpleProvinciaAdministrar implements ProvinciaAdministra{

	private static final long serialVersionUID = 1L;
	
	@Autowired
	private ProvinciaDAO provinciaDAO;
	
	public void setProviciaDao(ProvinciaDAO provinciaDAO) 
	{
		this.provinciaDAO=provinciaDAO;
	}
	
	public List<Provincia> getProvincias() {
		
		return provinciaDAO.getProvinciaList();
	}

	
}
