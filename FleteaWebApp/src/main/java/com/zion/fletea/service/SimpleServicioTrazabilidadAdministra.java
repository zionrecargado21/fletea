package com.zion.fletea.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.TrazabilidadServicio;
import com.zion.fletea.repository.TrazabilidadServicioDAO;

@Component
public class SimpleServicioTrazabilidadAdministra implements ServicioTrazabilidadAdministra {
	
	@Autowired
	private TrazabilidadServicioDAO trazabilidadDAO;
	
	@Override
	public void guardarServicio(TrazabilidadServicio servicio) {
		// TODO Auto-generated method stub
		trazabilidadDAO.guardarTrazabilidad(servicio);
	}
	
	@Override
	public TrazabilidadServicio getEstadoServicio(int idServicio) {
		return trazabilidadDAO.getEstadoServicio(idServicio);
	}

}
