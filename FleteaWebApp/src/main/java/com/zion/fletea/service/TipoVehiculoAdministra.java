package com.zion.fletea.service;
import java.util.List;

import com.zion.fletea.domain.TipoVehiculo;

public interface TipoVehiculoAdministra {

	public List<TipoVehiculo> getTipoVehiculos();
	
	public void guardarTipoVehiculo(TipoVehiculo tipovehiculo);
	
	public void borraTipoVehiculo(Integer idTipoVehiculo);
	
	public void updateTipoVehiculo(TipoVehiculo tipovehiculo);
	
	public TipoVehiculo getTipoVehiculo(int idTipoVehiculo);
	
	
}
