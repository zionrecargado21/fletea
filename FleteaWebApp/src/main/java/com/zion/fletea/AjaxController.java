package com.zion.fletea;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Localidad;
import com.zion.fletea.service.BarrioAdministra;
import com.zion.fletea.service.LocalidadAdministra;


/**
 * Handles requests for the application about page.
 */
@Controller
public class AjaxController {
	
	private static final Logger logger = LoggerFactory.getLogger(AboutController.class);
	
	@Autowired
	private LocalidadAdministra servicioLocalidad;
	
	@Autowired
	private BarrioAdministra servicioBarrio;
	
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(value = "/getLocalidadesByProvincia", method = RequestMethod.POST,  produces="application/json")
	@ResponseBody
	public List<Localidad> buscarLocalidades(HttpServletRequest request) {
		int idProvincia = Integer.parseInt(request.getParameter("idProvincia"));
		List<Localidad> localidad = (List<Localidad>) servicioLocalidad.getAllLocalidadByProvincia(idProvincia);
		return localidad;
	}
	
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(value = "/getBarriosByLocalidad", method = RequestMethod.POST,  produces="application/json")
	@ResponseBody
	public List<Barrio> buscarBarrio(HttpServletRequest request) {
		int idLocalidad = Integer.parseInt(request.getParameter("idLocalidad"));
		List<Barrio> barrio = (List<Barrio>) servicioBarrio.getAllBarrioByLocalidad(idLocalidad);
		return barrio;
	}
	
}
