package com.zion.fletea;


import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Administrador;
import com.zion.fletea.domain.Cliente;
import com.zion.fletea.domain.Fletero;
import com.zion.fletea.domain.Persona;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.UsuarioLogueado;
import com.zion.fletea.service.AdministradorAdministra;
import com.zion.fletea.service.ClienteAdministra;
import com.zion.fletea.service.FleteroAdministra;
import com.zion.fletea.service.ServicioAdministra;
import com.zion.fletea.service.TipoVehiculoAdministra;
import com.zion.fletea.service.VehiculoAdministra;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private ClienteAdministra clienteAdministra;
	
	@Autowired 
	private FleteroAdministra fleteroAdministra;
	
	@Autowired 
	private AdministradorAdministra administraAdministra;
	
	@Autowired 
	private ServicioAdministra servicioAdministra;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = {"/", "/home"}, method = RequestMethod.GET)
	public String home(Locale locale, Model model) {		
		return "index";
	}
	
	@RequestMapping(value = "/bienvenido", method = RequestMethod.GET)
	public ModelAndView bienvenido(HttpServletRequest request) {
		int idPersona = UsuarioLogueado.getId(request);

		ModelAndView model = new ModelAndView();
		Cliente cliente = clienteAdministra.getCliente(idPersona);
		Fletero fletero = fleteroAdministra.getFletero(idPersona);
		Administrador administrador = administraAdministra.getAdministrador(idPersona);
		
		
		if (cliente != null) {
			List<Servicio> serviciosCliente = servicioAdministra.getServicioByCliente(idPersona);
			model.addObject("serviciosCliente",serviciosCliente);
			model.addObject("cliente",cliente);
			model.setViewName("bienvenidoCliente");
		}
		
		if (fletero != null) {
			List<Servicio> serviciosFletero = servicioAdministra.getServicioByFletero(idPersona);
			model.addObject("fletero",fletero);
			model.addObject("serviciosFletero",serviciosFletero);
			model.setViewName("bienvenidoFletero");
		}
		
		if (administrador != null) {
			model.addObject("administrador",administrador);
			model.setViewName("bienvenido");
		}
		
		String usuario = request.getRemoteUser();
		model.addObject("usuario", usuario);
		return model;
	}
	
}
