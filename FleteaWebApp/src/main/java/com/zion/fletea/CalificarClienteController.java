package com.zion.fletea;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;


import com.zion.fletea.domain.CalificacionCliente;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.service.CalificacionClienteAdministra;
import com.zion.fletea.service.ServicioAdministra;

/**
 * Handles requests for the application about page.
 */
@Controller
public class CalificarClienteController {

	private static final Logger logger = LoggerFactory.getLogger(CalificarServicioController.class);

	@Autowired
	private ServicioAdministra servicioAdministra;

	@Autowired
	private CalificacionClienteAdministra calificacionClienteAdministra;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/calificarCliente", method = RequestMethod.GET)
	public ModelAndView calificarCliente(Model model, HttpServletRequest request) {
		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);

		CalificacionCliente calificacion = new CalificacionCliente();
		calificacion.setServicio(servicio);

		ModelAndView modelo = new ModelAndView("calificarCliente");
		modelo.addObject("calificacion", calificacion);
		return modelo;
	}

	@RequestMapping(value = "/guardarCalificacionCliente", method = RequestMethod.POST)
	public ModelAndView guardarCalificacionCliente(@ModelAttribute CalificacionCliente calificacion,
			BindingResult result, HttpServletRequest request) {

		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);
		calificacion.setServicio(servicio);
		
		if (calificacion.getIdCalificacionCliente() == 0) {
			Date dt = new Date();
			calificacion.setFecha(dt);
			calificacionClienteAdministra.guardarCalificacionCliente(calificacion);
		}

		return new ModelAndView("redirect:/verServicio?idServicio=" + calificacion.getServicio().getIdServicio());
	}

}