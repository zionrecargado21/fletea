package com.zion.fletea;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.RankingFleterosView;
import com.zion.fletea.domain.ServiciosConcretadosView;
import com.zion.fletea.repository.ReportesDAO;


@Controller
public class ReportesController {
	@RequestMapping(value = {"/ServiciosConcretados"}, method = RequestMethod.GET)
	public ModelAndView ServiciosConcretados(ModelAndView model) {		
		
		model.setViewName("serviciosConcretados");
		return model;
	}
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(value = "/ServiciosConcretadosJSON", method = RequestMethod.GET,  produces="application/json")
	@ResponseBody
	public List<ServiciosConcretadosView> serviciosConcretadosJSON(HttpServletRequest request) {
				
		String strDesde = request.getParameter("desde");
		String strHasta = request.getParameter("hasta");
		
		Date sqlDesde = strToDateSql(strDesde);
		Date sqlHasta = strToDateSql(strHasta);
		
		List<ServiciosConcretadosView> salida = ReportesDAO.getServiciosConcretados(sqlDesde, sqlHasta);
		
		return salida;			
	}
	
	private Date strToDateSql(String strFecha) {
	    SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        java.util.Date parsed;
		try {
			parsed = format.parse(strFecha);
		} catch (ParseException e) {
			return null;
		}
        java.sql.Date sql = new java.sql.Date(parsed.getTime());
        return sql;
	}
	
	@RequestMapping(value = {"/RankingFleteros"}, method = RequestMethod.GET)
	public ModelAndView RankingFleteros(ModelAndView model) {		
	
		model.setViewName("rankingFleteros");
		return model;
	}
	
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(value = "/RankingFleterosJSON", method = RequestMethod.GET,  produces="application/json")
	@ResponseBody
	public List<RankingFleterosView> rankingFleterosJSON(HttpServletRequest request) {
				
		Integer top = Integer.parseInt(request.getParameter("top")) ;

		List<RankingFleterosView> salida = ReportesDAO.getRankingFleteros(top);
		
		return salida;			
	}
	
}
