package com.zion.fletea;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Dia;
import com.zion.fletea.domain.DisponibilidadFletero;
import com.zion.fletea.domain.Fletero;
import com.zion.fletea.domain.UsuarioLogueado;
import com.zion.fletea.service.DisponibilidadFleteroAdministra;
import com.zion.fletea.service.FleteroAdministra;

@Controller
public class DisponibilidadFleteroController {
	
	@Autowired
	private DisponibilidadFleteroAdministra disponibilidadFleteroAdministra;
	
	@Autowired
	private FleteroAdministra fleteroAdministra;

	@ModelAttribute("allDias")
	public List<Dia> populateDias() {
		List<Dia> dia = disponibilidadFleteroAdministra.getAllDias();
		return dia;
	}

	
	@RequestMapping(value = "/registrarDisponibilidadFletero", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView model, HttpServletRequest request) {
		
		DisponibilidadFletero disponibilidadFletero = new DisponibilidadFletero();
		
		Fletero f = new Fletero();

		int id = UsuarioLogueado.getId(request);
		f.setIdPersona(id);


		
		disponibilidadFletero.setFletero(f);
		model.addObject("disponibilidadFletero", disponibilidadFletero);
		model.setViewName("registrarDisponibilidadFletero");
		return model;
	}
	
	@RequestMapping(value = "/listadoDisponibilidadFletero", method = RequestMethod.GET)
	public ModelAndView listadoDisponibilidadFletero(ModelAndView model, HttpServletRequest request) throws IOException {
		
		int id = UsuarioLogueado.getId(request);
		Fletero fletero = fleteroAdministra.getFletero(id);
		List<DisponibilidadFletero> listaFletero = disponibilidadFleteroAdministra.getDisponibilidadPorFletero(fletero.getIdPersona());
		model.addObject("fletero", fletero);
		model.addObject("listaDisponibilidadFletero", listaFletero);
		model.setViewName("listadoDisponibilidadFletero");
		return model;
	}
	
	@RequestMapping(value = "/guardarDisponibilidadFletero", method = RequestMethod.POST)
	public ModelAndView guardarDisponibilidadFletero(@ModelAttribute DisponibilidadFletero disponibilidadFletero, BindingResult result) {


//			System.out.println("P.toString: " + disponibilidadFletero.toString());

			disponibilidadFleteroAdministra.guardarDisponibilidadFletero(disponibilidadFletero);  
			

		return new ModelAndView("redirect:/listadoDisponibilidadFletero");
	}
	
	@RequestMapping(value = "/borrarDisponibilidadFletero", method = RequestMethod.GET)
	public ModelAndView borrarDisponibilidadFletero(HttpServletRequest request) {
		int idPersona = Integer.parseInt(request.getParameter("idPersona"));
		int idDia = Integer.parseInt(request.getParameter("idDia"));
		int horaDesde = Integer.parseInt(request.getParameter("horaDesde"));
		int horaHasta = Integer.parseInt(request.getParameter("horaHasta"));
		disponibilidadFleteroAdministra.borraDisponibilidadFletero(idPersona, idDia, horaDesde, horaHasta);
		return new ModelAndView("redirect:/listadoDisponibilidadFletero");
	}
	
	@ResponseStatus(value=HttpStatus.OK)
	@RequestMapping(value = "/getDisponibilidadFletero", method = RequestMethod.GET,  produces="application/json")
	@ResponseBody
	public List<DisponibilidadFletero> buscarDisponibilidadFletero(HttpServletRequest request) {
		int idFletero = Integer.parseInt(request.getParameter("idFletero"));
		return disponibilidadFleteroAdministra.getDisponibilidadPorFletero(idFletero);
	}
	
}
