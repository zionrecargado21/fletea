package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Cliente;


@Repository(value = "ClienteDAO")
public class JPAClienteDAO implements ClienteDAO{

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Cliente> getClienteList() {
		return em.createQuery("from Cliente", Cliente.class).getResultList();
	}
	
	
	@Transactional(readOnly = false) 
	public void guardarCliente(Cliente cliente) {
		em.persist(cliente);
		
	}

	
	@Transactional(readOnly = false)
	public void actualizarCliente(Cliente cliente) {
		em.merge(cliente);
		
	}

	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Cliente getCliente(int clienteid) {
		Query q = em.createQuery("from Cliente p where p.id = :idCliente");
		q.setParameter("idCliente", clienteid);
		
		try {
			Cliente p = (Cliente) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

	

	

}
