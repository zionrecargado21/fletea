package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Mensaje;

public interface MensajeDAO {
	
	public List<Mensaje> getAllMensajes(int idServicio);
	
	public void guardarMensaje(Mensaje mensaje);

}
