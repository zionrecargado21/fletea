package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Marca;


@Repository(value = "MarcaDAO")
public class JPAMarcaDAO implements MarcaDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Marca> getAllMarca() {
		return em.createQuery("from Marca", Marca.class).getResultList();
	}

	@Transactional
	public void guardarMarca(Marca marca) {
		em.persist(marca);
	}

	@Transactional(readOnly = false)
	public void borrarMarca(Integer marca) {
		Marca t = getMarca(marca);
		if (t != null) {

			Marca e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional
	public void updateMarca(Marca marca) {
		em.merge(marca);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Marca getMarca(int idMarca) {
		Query q = em.createQuery("from Marca p where p.idMarca = :idMarca");
		q.setParameter("idMarca", idMarca);

		try {
			Marca p = (Marca) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

}
