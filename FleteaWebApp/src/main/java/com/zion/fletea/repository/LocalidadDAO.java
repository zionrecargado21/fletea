package com.zion.fletea.repository;

import java.util.List;
import com.zion.fletea.domain.Localidad;


public interface LocalidadDAO {
	
	public List<Localidad> getAllLocalidad();
	
	public List<Localidad> getAllLocalidadByProvincia(int idProvincia);
	
	public void guardarLocalidad(Localidad localidad);

	public void borrarLocalidad(Integer idLocalidad);

	public void updateLocalidad(Localidad localidad);

	public Localidad getLocalidad(int idLocalidad);
	
}
