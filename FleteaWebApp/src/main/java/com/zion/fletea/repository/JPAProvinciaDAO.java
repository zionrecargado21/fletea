package com.zion.fletea.repository;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Provincia;
@Repository(value = "ProvinciaDAO")
public class JPAProvinciaDAO implements ProvinciaDAO {

		private EntityManager em = null;
		
	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	@Transactional(readOnly=true)
	@SuppressWarnings("unchecked")
	public List<Provincia> getProvinciaList() {
		// TODO Auto-generated method stub
		try {
			return em.createQuery("FROM Provincia p").getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@Transactional(readOnly=false)
	@Override
	public void guardarProvincia(Provincia provincia) {
		// TODO Auto-generated method stub
		em.merge(provincia);

	}

}
