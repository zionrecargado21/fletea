package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Dia;


public interface DiaDAO {
	
	public List<Dia> getAllDia();
	
	public Dia getDia(int idDia);
	
	
}