package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Modelo;
import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.domain.Vehiculo;

@Repository(value = "VehiculoDAO")
public class JPAVehiculoDAO implements VehiculoDAO {
	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Vehiculo> getVehiculosList() {
		return em.createQuery("FROM Vehiculo").getResultList();
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Vehiculo getVehiculo(int idVehiculo) {
		Query q = em.createQuery("from Vehiculo p where p.idVehiculo = :idVehiculo");
		q.setParameter("idVehiculo", idVehiculo);

		try {
			Vehiculo p = (Vehiculo) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

	@Transactional(readOnly = false)
	public void guardarVehiculo(Vehiculo vehiculo) {
		em.merge(vehiculo);

	}

	@Transactional(readOnly = false)
	public void actualizarVehiculo(Vehiculo vehiculo) {
		em.merge(vehiculo);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Vehiculo> getVehiculoPorFleterosYTipoVehiculo(List<Integer> filtrado, TipoVehiculo tipoVehiculo) {
		Query q = em
				.createQuery("from Vehiculo p where p.tipoVehiculo = :tipoVehiculo and p.fletero.idPersona IN (:ids)");
		q.setParameter("ids", filtrado);
		q.setParameter("tipoVehiculo", tipoVehiculo);

		try {
			System.out.println(q);
			List<Vehiculo> p = q.getResultList();
			return p;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<TipoVehiculo> getAllTipoVehiculo() {
		String q = "select m from TipoVehiculo m order by m.idTipoVehiculo";
		return em.createQuery(q).getResultList();
	}

	public TipoVehiculo getTipoVehiculoPorId(Integer id) {
		return em.find(TipoVehiculo.class, id);
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Modelo> getAllModelo() {
		String q = "select m from Modelo m order by m.idModeloVehiculo";
		return em.createQuery(q).getResultList();
	}

	public Modelo getModeloPorId(Integer id) {
		return em.find(Modelo.class, id);
	}

	@Transactional(readOnly = false)
	public void borrarVehiculo(int idVehiculo) {
		Vehiculo t = getVehiculo(idVehiculo);
		if (t != null) {

			Vehiculo e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Vehiculo> getVehiculosPorFleteroList(Integer idPersona) {
		Query q = em.createQuery("from Vehiculo p where p.fletero.idPersona = :idPersona");
		q.setParameter("idPersona", idPersona);

		try {
			List<Vehiculo> p =  q.getResultList();
			return p;
		} catch (Exception e) {
			return null;
		}

	}

}
