package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.CalificacionCliente;


public interface CalificacionClienteDAO {
	
	public List<CalificacionCliente> getAllCalificacionesCliente();
	
	
	public void guardarCalificacionCliente(CalificacionCliente calificacionCliente);

	public void borrarCalificacionCliente(Integer calificacionCliente);

	public void updateCalificacionCliente(CalificacionCliente calificacion);

	public CalificacionCliente getCalificacionCliente(int idCalificacionCliente);
	
	public CalificacionCliente getCalificacionClienteServicio(int idServicio);
}
