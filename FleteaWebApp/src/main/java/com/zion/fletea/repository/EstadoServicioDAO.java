package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.EstadoServicio;

public interface EstadoServicioDAO {
	
	public List<EstadoServicio> getAllEstadoServicios();
	
	public void guardarEstadoServicio(EstadoServicio estadoServicio);

	public void borrarEstadoServicio(Integer idEstadoServicio);

	public void actualizarEstadoServicio(EstadoServicio estadoServicio);

	public EstadoServicio getEstadoServicio(int idEstadoServicio);

}
