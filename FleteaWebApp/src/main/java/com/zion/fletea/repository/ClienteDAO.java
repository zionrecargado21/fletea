package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Cliente;
import com.zion.fletea.domain.Vehiculo;

public interface ClienteDAO {
	
	public List<Cliente> getClienteList();
	public void guardarCliente(Cliente cliente);
	public void actualizarCliente(Cliente cliente);
	public Cliente getCliente (int clienteid);

}
