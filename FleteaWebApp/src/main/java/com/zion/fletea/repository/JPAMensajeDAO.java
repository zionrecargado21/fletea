package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Mensaje;


@Repository(value = "MensajeDAO")
public class JPAMensajeDAO implements MensajeDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Mensaje> getAllMensajes(int idServicio) {

		Query q = em.createQuery("from Mensaje p where p.idServicio = " + idServicio, Mensaje.class);
		
		try {
			System.out.println(q.getResultList());
			return (List<Mensaje>) q.getResultList();
		}catch (Exception e) {
			System.out.println("esta vacio? " + idServicio);
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@Transactional
	public void guardarMensaje(Mensaje mensaje) {
		try {

			System.out.println("P.toString: " + mensaje.toString());
			em.persist(mensaje);
		}catch (Exception e) {

			System.out.println(e.getMessage());
		}
		
	}
	

}
