package com.zion.fletea.repository;

import java.util.List;
import com.zion.fletea.domain.Provincia;

public interface ProvinciaDAO {
	
	public List<Provincia> getProvinciaList();
	
	public void guardarProvincia(Provincia provincia);

}
