package com.zion.fletea.repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Administrador;

@Repository(value = "AdministradorDAO")
public class JPAAdministradorDAO implements AdministradorDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Administrador getAdministrador(int idPersona) {
		Query q = em.createQuery("from Administrador p where p.idPersona = :idPersona");
		q.setParameter("idPersona", idPersona);
		
		try {
			Administrador p = (Administrador) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

}
