package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Marca;

public interface MarcaDAO {
	
	public List<Marca> getAllMarca();
	
	public void guardarMarca(Marca marca);

	public void borrarMarca(Integer marca);

	public void updateMarca(Marca marca);

	public Marca getMarca(int idMarca);

}
