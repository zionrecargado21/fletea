package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.Query;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Fletero;


public interface FleteroDAO {
	
	public List<Fletero> getAllFletero();
	
	public List <Barrio> getAllBarrios();
	
	
	public void guardarFletero(Fletero fletero);

	public void borrarFletero(Integer fletero);

	public void updateFletero(Fletero fletero);

	public Fletero getFletero(int idFletero);

	public List<Fletero> getFleteroByName(String partialName);

	public List<Integer> getFleteroPorNombreYBarrio(String nombreFletero, Barrio idBarrio);

}