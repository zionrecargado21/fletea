package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.Query;

import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.domain.Vehiculo;

public interface VehiculoDAO {
	
	public List<Vehiculo> getVehiculosList();
	public void guardarVehiculo(Vehiculo vehiculo);
	public void actualizarVehiculo(Vehiculo vehiculo);
	public Vehiculo getVehiculo(int idVehiculo);
	public List<Vehiculo> getVehiculoPorFleterosYTipoVehiculo(List<Integer> filtrado, TipoVehiculo tipoVehiculo);
	public void borrarVehiculo(int idVehiculo);
	public List<Vehiculo> getVehiculosPorFleteroList(Integer idPersona);
}
