package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Localidad;

@Repository(value = "LocalidadDAO")
public class JPALocalidadDAO implements LocalidadDAO {
	
	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	public List<Localidad> getAllLocalidad() {	
		try {
			List<Localidad> lista = em.createQuery("from Localidad", Localidad.class).getResultList();		
			return lista;
		} catch (Exception e) {
			System.out.print(e.getMessage());
			return null;
		}
		
	}


	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Localidad> getAllLocalidadByProvincia(int idProvincia) {
				
		try {
			Query q = em.createQuery("from Localidad p where p.provincia.idProvincia = " + idProvincia, Localidad.class);
			System.out.println(q.getResultList());
			return (List<Localidad>) q.getResultList();
		}catch (Exception e) {
			System.out.println("esta vacio? " + idProvincia);
			System.out.println(e.getMessage());
			return null;
		}
		
	}

	@Override
	public void guardarLocalidad(Localidad localidad) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void borrarLocalidad(Integer idLocalidad) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void updateLocalidad(Localidad localidad) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Localidad getLocalidad(int idLocalidad) {
		// TODO Auto-generated method stub
		return null;
	}

}
