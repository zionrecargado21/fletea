package com.zion.fletea.repository;

import java.util.List;
import com.zion.fletea.domain.TipoVehiculo;

public interface TipoVehiculoDAO {
	
	public List<TipoVehiculo> getAllTipoVehiculo();
	
	public void guardarTipoVehiculo(TipoVehiculo tipovehiculo);

	public void borrarTipoVehiculo(Integer tipovehiculo);

	public void updateTipoVehiculo(TipoVehiculo tipovehiculo);

	public TipoVehiculo getTipoVehiculo(int idTipoVehiculo);

}
