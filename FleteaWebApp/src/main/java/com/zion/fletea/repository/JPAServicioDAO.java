package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.TipoVehiculo;

@Repository(value = "ServicioDAO")
public class JPAServicioDAO implements ServicioDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly=false)
	@Override
	public void guardarServicio(Servicio servicio) {
		try {
			System.out.println("P.toString: " + servicio.toString());
			em.persist(servicio);
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	@Transactional(readOnly = false)
	public void borrarServicio(Servicio servicio) {
		if (servicio != null) {
			Servicio e = em.merge(servicio);
			em.remove(e);
		}
	}

	@Transactional(readOnly = false)
	public void updateServicio(Servicio servicio) {
		// TODO Auto-generated method stub
		em.merge(servicio);
	}

	@Transactional(readOnly = true)
	public Servicio getServicio(int idServicio) {

		Query q = em.createQuery("from Servicio p where p.idServicio = " + idServicio, Object[].class);

		try {
			System.out.println(q.getFirstResult());
			return (Servicio) q.getSingleResult();
		} catch (Exception e) {
			System.out.println("esta vacio? " + idServicio);
			System.out.println(e.getMessage());
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	public List<Servicio> getServicioByCliente(int idPersona) {
		
		Query q = em.createQuery("from Servicio p where p.cliente.idPersona = " + idPersona, Servicio.class);

		try {
			System.out.println(q.getResultList());
			return (List<Servicio>) q.getResultList();
		} catch (Exception e) {
			System.out.println("esta vacio? " + idPersona);
			System.out.println(e.getMessage());
			return null;
		}
		
	}
	
	@Transactional(readOnly = true)
	public List<Servicio> getServicioByFletero(int idPersona) {
		
		Query q = em.createQuery("from Servicio p where p.fletero.idPersona = " + idPersona, Servicio.class);

		try {
			System.out.println(q.getResultList());
			return (List<Servicio>) q.getResultList();
		} catch (Exception e) {
			System.out.println("esta vacio? " + idPersona);
			System.out.println(e.getMessage());
			return null;
		}
		
	}

}
