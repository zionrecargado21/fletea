package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.TipoVehiculo;

@Repository(value = "tipovehiculoDAO")
public class JPATipoVehiculoDAO implements TipoVehiculoDAO{

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<TipoVehiculo> getAllTipoVehiculo() {
		return em.createQuery("from TipoVehiculo", TipoVehiculo.class).getResultList();
	}

	@Transactional
	public void guardarTipoVehiculo(TipoVehiculo tipovehiculo) {
		
		em.persist(tipovehiculo);
		
	}

	@Transactional(readOnly = false)
	public void borrarTipoVehiculo(Integer tipovehiculo) {
		
		TipoVehiculo t = getTipoVehiculo(tipovehiculo);
		if (t != null) {
			
			TipoVehiculo e = em.merge(t);
			em.remove(e);

		}
		
	}
	
	@Transactional(readOnly = false)
	public void updateTipoVehiculo(TipoVehiculo tipovehiculo) {

		em.merge(tipovehiculo);
		
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public TipoVehiculo getTipoVehiculo(int idTipoVehiculo) {
		Query q = em.createQuery("from TipoVehiculo p where p.idTipoVehiculo = :idTipoVehiculo");
		q.setParameter("idTipoVehiculo", idTipoVehiculo);
		
		try {
			TipoVehiculo p = (TipoVehiculo) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

}
