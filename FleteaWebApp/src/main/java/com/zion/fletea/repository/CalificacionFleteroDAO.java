package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.CalificacionFletero;


public interface CalificacionFleteroDAO {
	
	public List<CalificacionFletero> getAllCalificacionesFletero();
	
	
	public void guardarCalificacionFletero(CalificacionFletero calificacionFletero);

	public void borrarCalificacionFletero(Integer calificacionFletero);

	public void updateCalificacionFletero(CalificacionFletero calificacion);

	public CalificacionFletero getCalificacionFletero(int idCalificacionFletero);

	public double getCalificacionPromedioFletero(int idFletero);

	public CalificacionFletero getCalificacionFleteroServicio(int idServicio);
}
