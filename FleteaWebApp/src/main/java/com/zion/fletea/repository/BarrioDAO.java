package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Barrio;


public interface BarrioDAO {
	public List<Barrio> getAllBarrio();
	
	public void guardarBarrio(Barrio barrio);

	public void borrarBarrio(Integer barrio);

	public void updateBarrio(Barrio barrio);

	public Barrio getBarrio(int idBarrio);

	public List<Barrio> getAllBarrioByLocalidad(int idLocalidad);
}
