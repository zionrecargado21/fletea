package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.zion.fletea.domain.CalificacionCliente;

@Repository(value = "CalificacionClienteDAO")
public class JPACalificacionClienteDAO implements CalificacionClienteDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<CalificacionCliente> getAllCalificacionesCliente() {
		List<CalificacionCliente> lista = em.createQuery("from CalificacionCliente", CalificacionCliente.class)
				.getResultList();
		return lista;
	}

	@Transactional(readOnly = false)
	@SuppressWarnings("unchecked")
	public void guardarCalificacionCliente(CalificacionCliente calificacionCliente) {
		em.persist(calificacionCliente);
	}

	@Transactional(readOnly = false)
	@SuppressWarnings("unchecked")
	public void borrarCalificacionCliente(Integer calificacionCliente) {
		CalificacionCliente t = getCalificacionCliente(calificacionCliente);
		if (t != null) {

			CalificacionCliente e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional
	public void updateCalificacionCliente(CalificacionCliente calificacion) {
		em.merge(calificacion);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public CalificacionCliente getCalificacionCliente(int idCalificacionCliente) {
		Query q = em.createQuery("from CalificacionCliente p where p.idCalificacionCliente = :idCalificacionCliente");
		q.setParameter("idCalificacionCliente", idCalificacionCliente);

		try {
			CalificacionCliente p = (CalificacionCliente) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public CalificacionCliente getCalificacionClienteServicio(int idServicio) {
		Query q = em.createQuery("from CalificacionCliente p where p.servicio.idServicio = :idServicio");
		q.setParameter("idServicio", idServicio);

		try {
			CalificacionCliente p = (CalificacionCliente) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

}
