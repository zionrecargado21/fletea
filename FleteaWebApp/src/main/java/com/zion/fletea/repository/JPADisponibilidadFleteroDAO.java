package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Dia;
import com.zion.fletea.domain.DisponibilidadFletero;
import com.zion.fletea.domain.Fletero;

@Repository(value = "DisponibilidadFleteroDAO")
public class JPADisponibilidadFleteroDAO implements DisponibilidadFleteroDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<DisponibilidadFletero> getAllDisponibilidadFletero() {
		List<DisponibilidadFletero> a = em.createQuery("FROM DisponibilidadFletero").getResultList();
		return a;
	}

	@Override
	@Transactional
	public void guardarDisponibilidadFletero(DisponibilidadFletero disponibilidadFletero) {
		Fletero fletero = em.find(Fletero.class, disponibilidadFletero.getFletero().getIdPersona());
		Dia dia = em.find(Dia.class, disponibilidadFletero.getDia().getIdDia());

		DisponibilidadFletero df = new DisponibilidadFletero();
		df.setDia(dia);
		df.setFletero(fletero);
		df.setHoraDesde(disponibilidadFletero.getHoraDesde());
		df.setHoraHasta(disponibilidadFletero.getHoraHasta());

		em.persist(df);

	}

	@Override
	@Transactional(readOnly = false)
	public void borrarDisponibilidadFletero(Integer fletero, Integer dia, Integer horaDesde, Integer horaHasta) {

		DisponibilidadFletero f = getDisponibilidadFletero(fletero, dia, horaDesde, horaHasta);
		if (f != null) {

			DisponibilidadFletero e = em.merge(f);
			em.remove(e);

		}

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Dia> getAllDias() {
		String q = "select d from Dia d order by d.idDia";
		return em.createQuery(q).getResultList();
	}
	// public Barrio getBarrioPorId(Integer id) {
	// return em.find(Barrio.class, id);
	// }

	@Transactional(readOnly = true)
	@Override
	public DisponibilidadFletero getDisponibilidadFletero(Integer idPersona, Integer idDia, Integer horaDesde,
			Integer horaHasta) {
		Query q = em.createQuery(
				"from DisponibilidadFletero p where p.fletero.idPersona = :idPersona and p.dia.idDia = :idDia and p.horaDesde = :horaDesde and p.horaHasta = :horaHasta ");

		q.setParameter("horaDesde", horaDesde);
		q.setParameter("horaHasta", horaHasta);
		q.setParameter("idPersona", idPersona);
		q.setParameter("idDia", idDia);

		try {
			DisponibilidadFletero p = (DisponibilidadFletero) q.getSingleResult();
			return p;
		} catch (Exception e) {

			return null;
		}
	}

	@Override
	public List<DisponibilidadFletero> getDisponibilidadPorFletero(int idFletero) {
		Query q = em.createQuery(
				"from DisponibilidadFletero p where p.fletero.idPersona = :idPersona");
		q.setParameter("idPersona", idFletero);

		try {
			List<DisponibilidadFletero> p = q.getResultList();
			return p;
			
		} catch (Exception e) {

			return null;
		}

	}
}