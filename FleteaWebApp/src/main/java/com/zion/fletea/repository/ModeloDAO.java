package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;

public interface ModeloDAO {
	
	public List<Modelo> getAllModelo();
	
	public List <Marca> getAllMarcas();
	
	public void guardarModelo(Modelo modelo);

	public void borrarModelo(Integer modelo);

	public void updateModelo(Modelo modelo);

	public Modelo getModelo(int idModelo);

}
