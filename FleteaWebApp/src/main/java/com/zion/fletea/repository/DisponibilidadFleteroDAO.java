package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Dia;
import com.zion.fletea.domain.DisponibilidadFletero;


public interface DisponibilidadFleteroDAO {
	
	public List<DisponibilidadFletero> getAllDisponibilidadFletero();
	
	public List <Dia> getAllDias();
		
	public void guardarDisponibilidadFletero(DisponibilidadFletero disponibilidadFletero);

	public void borrarDisponibilidadFletero(Integer fletero, Integer dia, Integer horaDesde, Integer horaHasta);

	public DisponibilidadFletero getDisponibilidadFletero(Integer idPersona, Integer idDia, Integer horaDesde, Integer horaHasta);

	public List<DisponibilidadFletero> getDisponibilidadPorFletero(int idFletero);
}