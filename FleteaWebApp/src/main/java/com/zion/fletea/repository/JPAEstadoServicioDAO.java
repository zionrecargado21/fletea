package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.EstadoServicio;
import com.zion.fletea.domain.Marca;

@Repository(value = "EstadoServicioDAO")
public class JPAEstadoServicioDAO implements EstadoServicioDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional
	public void guardarEstadoServicio(EstadoServicio estadoServicio) {
		em.persist(estadoServicio);

	}

	@Transactional(readOnly = false)
	public void borrarEstadoServicio(Integer idEstadoServicio) {
		EstadoServicio t = getEstadoServicio(idEstadoServicio);
		if (t != null) {

			EstadoServicio e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<EstadoServicio> getAllEstadoServicios() {
		return em.createQuery("from EstadoServicio", EstadoServicio.class).getResultList();
	}

	@Transactional
	public void actualizarEstadoServicio(EstadoServicio estadoServicio) {
		em.merge(estadoServicio);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public EstadoServicio getEstadoServicio(int idEstadoServicio) {
		Query q = em.createQuery("from EstadoServicio p where p.idEstadoServicio = :idEstadoServicio");
		q.setParameter("idEstadoServicio", idEstadoServicio);

		try {
			EstadoServicio p = (EstadoServicio) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

}
