package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Dia;

@Repository(value = "DiaDAO")
public class JPADiaDAO implements DiaDAO {
	
	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}
	

	@Transactional(readOnly = true)
	public List<Dia> getAllDia() {		
		List<Dia> lista = em.createQuery("from Dia ORDER BY idDia", Dia.class).getResultList();		
		return lista;
	}
	

	@Override
	public Dia getDia(int idDia) {
		// TODO Auto-generated method stub
		return null;
	}



}
