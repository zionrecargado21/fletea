package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;

@Repository(value = "ModeloDAO")
public class JPAModeloDAO implements ModeloDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Modelo> getAllModelo() {
		return em.createQuery("from Modelo", Modelo.class).getResultList();
	}

	@Transactional
	public void guardarModelo(Modelo modelo) {
		modelo.setMarca(getMarcaPorId(modelo.getMarca().getIdMarca())); 
		em.persist(modelo);

	}

	@Transactional(readOnly = false)
	public void borrarModelo(Integer modelo) {

		Modelo t = getModelo(modelo);
		if (t != null) {

			Modelo e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional
	public void updateModelo(Modelo modelo) {
		em.merge(modelo);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Modelo getModelo(int idModelo) {
		Query q = em.createQuery("from Modelo p where p.idModeloVehiculo = :idModelo");
		q.setParameter("idModelo", idModelo);

		try {
			Modelo p = (Modelo) q.getSingleResult();
			return p;
		} catch (Exception e) {
			
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Marca> getAllMarcas() {
		String q = "select m from marca m order by m.idMarca";
		return em.createQuery(q).getResultList();
	}

	public Marca getMarcaPorId(Integer id) {
		return em.find(Marca.class, id);
	}


}
