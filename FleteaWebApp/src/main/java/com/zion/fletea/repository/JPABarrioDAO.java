package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Localidad;

@Repository(value = "BarrioDAO")
public class JPABarrioDAO implements BarrioDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	public List<Barrio> getAllBarrio() {
		List<Barrio> lista = em.createQuery("from Barrio", Barrio.class).getResultList();
		return lista;
	}

	@Override
	public void guardarBarrio(Barrio barrio) {
		// TODO Auto-generated method stub

	}

	@Override
	public void borrarBarrio(Integer barrio) {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateBarrio(Barrio barrio) {
		// TODO Auto-generated method stub

	}

	@Transactional(readOnly = true)
	public Barrio getBarrio(int idBarrio) {
		try {
			Query q = em.createQuery("from Barrio p where p.idBarrio = " + idBarrio, Barrio.class);
			System.out.println(q.getResultList());
			return (Barrio) q.getSingleResult();
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<Barrio> getAllBarrioByLocalidad(int idLocalidad) {

		try {
			Query q = em.createQuery("from Barrio p where p.localidad.idLocalidad = " + idLocalidad, Barrio.class);
			System.out.println(q.getResultList());
			return (List<Barrio>) q.getResultList();
		} catch (Exception e) {
			System.out.println("idLocalidad: " + idLocalidad);
			System.out.println(e.getMessage());
			return null;
		}

	}

}
