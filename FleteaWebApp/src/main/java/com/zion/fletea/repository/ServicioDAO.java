package com.zion.fletea.repository;

import java.util.List;

import com.zion.fletea.domain.Servicio;

public interface ServicioDAO {
	
	
	public void guardarServicio(Servicio servicio);

	public void borrarServicio(Servicio servicio);

	public void updateServicio(Servicio servicio);

	public Servicio getServicio(int idServicio);
	
	public List<Servicio> getServicioByCliente(int idPersona);
	
	public List<Servicio> getServicioByFletero(int idPersona);

}
