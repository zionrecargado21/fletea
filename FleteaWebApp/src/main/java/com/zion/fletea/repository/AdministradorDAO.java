package com.zion.fletea.repository;

import com.zion.fletea.domain.Administrador;

public interface AdministradorDAO {
	
	public Administrador getAdministrador(int idPersona);

}
