package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.CalificacionFletero;
import com.zion.fletea.domain.Marca;

@Repository(value = "CalificacionFleteroDAO")
public class JPACalificacionFleteroDAO implements CalificacionFleteroDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<CalificacionFletero> getAllCalificacionesFletero() {
		List<CalificacionFletero> lista = em.createQuery("from CalificacionFletero", CalificacionFletero.class)
				.getResultList();
		return lista;
	}

	@Transactional(readOnly = false)
	@SuppressWarnings("unchecked")
	public void guardarCalificacionFletero(CalificacionFletero calificacionFletero) {
		em.persist(calificacionFletero);
	}

	@Transactional(readOnly = false)
	@SuppressWarnings("unchecked")
	public void borrarCalificacionFletero(Integer calificacionFletero) {
		CalificacionFletero t = getCalificacionFletero(calificacionFletero);
		if (t != null) {

			CalificacionFletero e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional
	public void updateCalificacionFletero(CalificacionFletero calificacion) {
		em.merge(calificacion);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public CalificacionFletero getCalificacionFletero(int idCalificacionFletero) {
		Query q = em.createQuery("from CalificacionFletero p where p.idCalificacionFletero = :idCalificacionFletero");
		q.setParameter("idCalificacionFletero", idCalificacionFletero);

		try {
			CalificacionFletero p = (CalificacionFletero) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public CalificacionFletero getCalificacionFleteroServicio(int idServicio) {
		Query q = em.createQuery("from CalificacionFletero p where p.servicio.idServicio = :idServicio");
		q.setParameter("idServicio", idServicio);

		try {
			CalificacionFletero p = (CalificacionFletero) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return null;
		}
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public double getCalificacionPromedioFletero(int idFletero) {
		Query q = em.createQuery("select avg(p.cantEstrellas) from CalificacionFletero p where p.servicio.fletero.idPersona = :idFletero");
		q.setParameter("idFletero", idFletero);

		try {
			double p =  (Double) q.getSingleResult();
			return p;
		} catch (Exception e) {
			return 0;
		}
		

	}

}
