package com.zion.fletea.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.Date;

import javax.servlet.http.HttpServletRequest;

import com.zion.fletea.domain.Constantes;
import com.zion.fletea.domain.RankingFleterosView;
import com.zion.fletea.domain.ServiciosConcretadosView;

public class ReportesDAO {

	public static List<ServiciosConcretadosView> getServiciosConcretados(Date desde, Date hasta) {
		
		   List<ServiciosConcretadosView> lista = new ArrayList<ServiciosConcretadosView>();

	
		   Connection conn = null;
		   PreparedStatement  stmt = null;
		   try{	
		      Class.forName("com.mysql.jdbc.Driver");		   
		      conn = DriverManager.getConnection(Constantes.DB_URL, Constantes.USER, Constantes.PASS);
		      
		      String sql = 
		    		    "select s.idServicio, p.nombre, tv.descripcion tipoVehiculo, l.nombre as localidad, prov.nombre as provincia, s.fechaSolicitud " + 
			      		"from servicio s " + 
			      		"inner join serviciotrazabilidad st " + 
			      		"  on s.idservicio = st.idServicio " + 
			      		"inner join persona p " + 
			      		"  on s.idPersonaFletero = p.idPersona  " + 
			      		"inner join vehiculo v  " + 
			      		"  on s.idVehiculo = v.idVehiculo " + 
			      		"inner join tipoVehiculo tv " + 
			      		"  on v.idTipoVehiculo = tv.idTipoVehiculo " + 
			      		"inner join barrio b " + 
			      		"  on s.idBarrio = b.idBarrio " + 
			      		"inner join localidad l " + 
			      		"  on b.idLocalidad = l.idLocalidad " + 
			      		"inner join provincia prov " + 
			      		"  on l.idProvincia = prov.idProvincia " + 
			      		"where st.idEstadoServicio = 4 " + 
			      		"and s.fechaSolicitud BETWEEN ? AND ? " + 
			      		"and p.dtype = 'Fletero' ;";	
		      
		      stmt = conn.prepareStatement(sql);		      		      	      
		      stmt.setDate(1, desde);
		      stmt.setDate(2, hasta);
		      
		      ResultSet rs = stmt.executeQuery();	
		      
		      while(rs.next()) {
		    	  ServiciosConcretadosView o = new ServiciosConcretadosView();
		    	  o.setIdServicio(rs.getInt("idServicio"));
		    	  o.setNombre(rs.getString("nombre"));
		    	  o.setTipoVehiculo(rs.getString("tipoVehiculo"));
		    	  o.setLocalidad(rs.getString("localidad"));
		    	  o.setProvincia(rs.getString("provincia"));
		    	  
		    	  java.util.Date f = rs.getTimestamp("fechaSolicitud");
		    	  o.setFechaSolicitud(f.toString());
		    	  
		    	  lista.add (o) ;
		      }
		      
		      
		      rs.close();		   
		   }catch(Exception e){
			  lista = null;	      
		   }finally{		     
		      try{
		         if(stmt!=null)
		        	 stmt.close();
		      }catch(SQLException se){
		      }
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		      }
		   }	   
		   return lista;
	}

	public static List<RankingFleterosView> getRankingFleteros(Integer top) {
		
		   List<RankingFleterosView> lista = new ArrayList<RankingFleterosView>();

	
		   Connection conn = null;
		   PreparedStatement  stmt = null;
		   try{	
		      Class.forName("com.mysql.jdbc.Driver");		   
		      conn = DriverManager.getConnection(Constantes.DB_URL, Constantes.USER, Constantes.PASS);
		      
		      String sql = 
		    		    "select count(*) as cantServicios, p.nombre, l.nombre as localidad, prov.nombre as provincia " + 
		    		    "from servicio s " + 
		    		    "inner join serviciotrazabilidad st " + 
		    		    "  on s.idservicio = st.idServicio " + 
		    		    "inner join persona p " + 
		    		    "  on s.idPersonaFletero = p.idPersona  " + 
		    		    "inner join barrio b " + 
		    		    "  on s.idBarrio = b.idBarrio " + 
		    		    "inner join localidad l " + 
		    		    "  on b.idLocalidad = l.idLocalidad " + 
		    		    "inner join provincia prov " + 
		    		    "  on l.idProvincia = prov.idProvincia " + 
		    		    "where st.idEstadoServicio = 4 " + 
		    		    "and p.dtype = 'Fletero'  " + 
		    		    "group by p.nombre, l.nombre, prov.nombre " + 
		    		    "order by 1 desc " + 
		    		    "limit 0,?;";	
		      
		      stmt = conn.prepareStatement(sql);		      		      	      
		      stmt.setInt(1, top);
		  
		      
		      ResultSet rs = stmt.executeQuery();	
		      
		      while(rs.next()) {
		    	  RankingFleterosView o = new RankingFleterosView();
		    	  o.setCantServicios(rs.getInt("cantServicios"));
		    	  o.setNombre(rs.getString("nombre"));
		    	  o.setLocalidad(rs.getString("localidad"));
		    	  o.setProvincia(rs.getString("provincia"));
		    
		    	  lista.add (o) ;
		      }
		      
		      
		      rs.close();		   
		   }catch(Exception e){
			  lista = null;	      
		   }finally{		     
		      try{
		         if(stmt!=null)
		        	 stmt.close();
		      }catch(SQLException se){
		      }
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		      }
		   }	   
		   return lista;
	}

}
