package com.zion.fletea.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Fletero;

@Repository(value = "FleteroDAO")
public class JPAFleteroDAO implements FleteroDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}


	@Override
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Fletero> getAllFletero() {
		return em.createQuery("FROM Fletero").getResultList();
	}

	@Override
	@Transactional
	public void guardarFletero(Fletero fletero) {
		
		em.persist(fletero);

	}

	@Override
	@Transactional(readOnly = false)
	public void borrarFletero(Integer fletero) {

		Fletero f = getFletero(fletero);
		if (f != null) {

			Fletero e = em.merge(f);
			em.remove(e);

		}

	}

	@Override
	@Transactional
	public void updateFletero(Fletero fletero) {
		em.merge(fletero);

	}

	@Transactional(readOnly = true)
	@Override
	public Fletero getFletero(int idFletero) {
		Query q = em.createQuery("from Fletero p where p.idPersona = :idFletero");
		q.setParameter("idFletero", idFletero);

		try {
			Fletero p = (Fletero) q.getSingleResult();
			return p;
		} catch (Exception e) {
			
			return null;
		}
	}
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Fletero> getFleteroByName(String partialName) {
		Query q = em.createQuery("from Fletero p where p.nombre LIKE CONCAT('%', :partialName, '%')");
		q.setParameter("partialName", partialName);

		try {
			return q.getResultList();
		} catch (Exception e) {
			
			return null;
		}
	}
	
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Barrio> getAllBarrios() {
		String q = "select m from Barrio m order by m.idBarrio";
		return em.createQuery(q).getResultList();
	}
	public Barrio getBarrioPorId(Integer id) {
		return em.find(Barrio.class, id);
	}


	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Integer> getFleteroPorNombreYBarrio(String nombreFletero, Barrio barrio) {
		Query q = em.createQuery("select idPersona from Fletero p where p.nombre LIKE CONCAT('%', :partialName, '%') and p.barrio = :barrio");
		q.setParameter("partialName", nombreFletero);
		q.setParameter("barrio",barrio);

		try {
			return q.getResultList();
		} catch (Exception e) { 
			System.out.println(e.getMessage());
			return null;
		}

	}
}