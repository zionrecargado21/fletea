package com.zion.fletea.repository;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.zion.fletea.domain.EstadoServicio;
import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.TrazabilidadServicio;

@Repository(value = "TrazabilidadServicioDAO")
public class JPATrazabilidadServicioDAO implements TrazabilidadServicioDAO {

	private EntityManager em = null;

	@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<TrazabilidadServicio> getAllTrazabilidadServicio() {
		return em.createQuery("from TrazabilidadServicio", TrazabilidadServicio.class).getResultList();
	}
	
	
	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public List<Servicio> getAllServicio() {
		String q = "select m from TrazabilidadServicio m order by m.fecha";
		return em.createQuery(q).getResultList();
	}
	
	@Transactional
	public void guardarModelo(Modelo modelo) {
		modelo.setMarca(getMarcaPorId(modelo.getMarca().getIdMarca())); 
		em.persist(modelo);

	}
	
	@Transactional
	public void guardarTrazabilidad(TrazabilidadServicio modelo) {
		em.persist(modelo);
	}
	
	@Transactional
	public void guardarTrazabilidadServicio(TrazabilidadServicio trazabilidadServicio) {
		trazabilidadServicio.setEstadoServicio(getEstadoServicioPorId(trazabilidadServicio.getEstadoServicio().getIdEstadoServicio()));
		trazabilidadServicio.setServicio(getServicioPorId(trazabilidadServicio.getServicio().getIdServicio()));
	}

	private Servicio getServicioPorId(Integer idServicio) {
		// TODO Auto-generated method stub
		return null;
	}

	private EstadoServicio getEstadoServicioPorId(Integer idEstadoServicio) {
		// TODO Auto-generated method stub
		return null;
	}

	@Transactional(readOnly = false)
	public void borrarModelo(Integer modelo) {

		Modelo t = getModelo(modelo);
		if (t != null) {

			Modelo e = em.merge(t);
			em.remove(e);

		}

	}

	@Transactional
	public void updateModelo(Modelo modelo) {
		em.merge(modelo);

	}

	@Transactional(readOnly = true)
	@SuppressWarnings("unchecked")
	public Modelo getModelo(int idModelo) {
		Query q = em.createQuery("from Modelo p where p.idModeloVehiculo = :idModelo");
		q.setParameter("idModelo", idModelo);

		try {
			Modelo p = (Modelo) q.getSingleResult();
			return p;
		} catch (Exception e) {
			
			return null;
		}
	}
	

	public Marca getMarcaPorId(Integer id) {
		return em.find(Marca.class, id);
	}


	

	

	@Override
	public void actualizarTrazabilidadServicio(TrazabilidadServicio trazabilidadServicio) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public TrazabilidadServicio getTrazabilidadServicio(int idServicio, int idEstadoServicio, Date fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TrazabilidadServicio getEstadoServicio(int idServicio) {
		// TODO Auto-generated method stub
		Query q = em.createQuery("from TrazabilidadServicio p where p.servicio.idServicio = :idServicio order by p.fecha DESC");
		q.setParameter("idServicio", idServicio);
		q.setMaxResults(1);

		try {
			TrazabilidadServicio p = (TrazabilidadServicio) q.getSingleResult();
			return p;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}
	}
}
