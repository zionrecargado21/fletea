package com.zion.fletea.repository;

import java.util.Date;
import java.util.List;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.TrazabilidadServicio;

public interface TrazabilidadServicioDAO {
	
	public List<TrazabilidadServicio> getAllTrazabilidadServicio();
	
	public List <Servicio> getAllServicio();
	
	public void guardarTrazabilidad(TrazabilidadServicio trazabilidadServicio);
	
	public void guardarTrazabilidadServicio(TrazabilidadServicio trazabilidadServicio);
	
	public void actualizarTrazabilidadServicio(TrazabilidadServicio trazabilidadServicio);

	public TrazabilidadServicio getTrazabilidadServicio(int idServicio, int idEstadoServicio, Date fecha);
	
	public TrazabilidadServicio getEstadoServicio(int idServicio);

}
