package com.zion.fletea;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Cliente;
import com.zion.fletea.domain.Fletero;
import com.zion.fletea.domain.Localidad;
import com.zion.fletea.domain.Provincia;
import com.zion.fletea.service.BarrioAdministra;
import com.zion.fletea.service.ClienteAdministra;
import com.zion.fletea.service.FleteroAdministra;
import com.zion.fletea.service.LocalidadAdministra;
import com.zion.fletea.service.ProvinciaAdministra;

/**
 * Handles requests for the application about page.
 */
@Controller
public class ClienteController {
	
	@Autowired
	private ClienteAdministra clienteAdministra;

	@Autowired
	private ProvinciaAdministra provinciaAdministra;

	@Autowired
	private BarrioAdministra barrioAdministra;

	@Autowired
	private LocalidadAdministra localidadAdministra;

	@RequestMapping(value = "/verMiUsuario", method = RequestMethod.GET)
	public ModelAndView verMiUsuario(HttpServletRequest request) {
		int idCliente = Integer.parseInt(request.getParameter("idCliente"));
		Cliente cliente = clienteAdministra.getCliente(idCliente);
		ModelAndView model = new ModelAndView("verMiUsuario");
		model.addObject("cliente", cliente);
		return model;
	}
	
	@RequestMapping(value = "/registrarCliente", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView model) {

		Cliente cliente = new Cliente();
		model.addObject("cliente", cliente);

		List<Provincia> provincias = provinciaAdministra.getProvincias();
		model.addObject("provincias", provincias);

		model.addObject("localidades", null);
		model.addObject("barrios", null);

		model.setViewName("registrarCliente");
		return model;
	}
	
	@RequestMapping(value = "/guardarCliente", method = RequestMethod.POST)
	public ModelAndView guardarCliente(@ModelAttribute Cliente cliente, BindingResult result,
			HttpServletRequest request) {
		
		int idBarrio = Integer.parseInt(request.getParameter("barrio"));
		Barrio barrio = barrioAdministra.getBarrio(idBarrio);
		cliente.setBarrio(barrio);

		if (cliente.getIdPersona() == null || cliente.getIdPersona() == 0) {
			System.out.println("P.toString: " + cliente.toString());
			clienteAdministra.guardarCliente(cliente);
		} else {
			clienteAdministra.updateCliente(cliente);
		}

		//return new ModelAndView("redirect:/listadoClientes");

		return new ModelAndView("redirect:/bienvenido");
	}

	@RequestMapping(value = "/editarCliente", method = RequestMethod.GET)
	public ModelAndView editarCliente(HttpServletRequest request) {

		int idCliente = Integer.parseInt(request.getParameter("idPersona"));
		Cliente cliente = clienteAdministra.getCliente(idCliente);
		ModelAndView model = new ModelAndView("registrarCliente");
		model.addObject("cliente", cliente);

		List<Provincia> provincias = provinciaAdministra.getProvincias();
		model.addObject("provincias", provincias);

		List<Localidad> localidad = (List<Localidad>) localidadAdministra
				.getAllLocalidadByProvincia(cliente.getBarrio().getLocalidad().getProvincia().getIdProvincia());
		model.addObject("localidades", localidad);

		List<Barrio> barrio = (List<Barrio>) barrioAdministra
				.getAllBarrioByLocalidad(cliente.getBarrio().getLocalidad().getIdLocalidad());
		model.addObject("barrios", barrio);

		return model;
	}
	
	@RequestMapping(value = "/listadoClientes", method = RequestMethod.GET)
	public ModelAndView listadoCliente(ModelAndView model) throws IOException {
		List<Cliente> listaCliente = clienteAdministra.getClientes();
		model.addObject("listaCliente", listaCliente);
		model.setViewName("listadoCliente");
		return model;
	}
}
