package com.zion.fletea;

import org.apache.velocity.runtime.directive.Foreach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Localidad;
import com.zion.fletea.domain.Provincia;
import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.domain.Vehiculo;
import com.zion.fletea.domain.VehiculoXReputacionWrapper;
import com.zion.fletea.service.SimpleBarrioAdministra;
import com.zion.fletea.service.SimpleCalificacionFleteroAdministra;
import com.zion.fletea.service.SimpleFleteroAdministra;
import com.zion.fletea.service.SimpleLocalidadAdministra;
import com.zion.fletea.service.SimpleProvinciaAdministrar;
import com.zion.fletea.service.TipoVehiculoAdministra;
import com.zion.fletea.service.VehiculoAdministra;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Handles requests for the application about page.
 */
@Controller
public class BusquedaServiciosController {

	//private static final Logger logger = LoggerFactory.getLogger(RegistrarTipoVehiculoController.class);

	@Autowired
	private VehiculoAdministra vehiculoAdministra;
	
	@Autowired
	private TipoVehiculoAdministra tipoVehiculoAdministra;
	
	@Autowired
	private SimpleLocalidadAdministra localidadAdministra;
	
	@Autowired
	private SimpleBarrioAdministra barrioAdministra;
	
	@Autowired
	private SimpleFleteroAdministra fleteroAdministra;
	
	@Autowired
	private SimpleProvinciaAdministrar provinciaAdministra;
	
	@Autowired
	private SimpleCalificacionFleteroAdministra calificionFleteroAdministra;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	
	@RequestMapping(value = "/busquedaServiciosDisponibles", method = RequestMethod.GET)
	public ModelAndView listadoVehiculos(ModelAndView model) throws IOException {
		List<Vehiculo> listaVehiculo = vehiculoAdministra.getVehiculosList();
		List<VehiculoXReputacionWrapper> listado = new LinkedList<VehiculoXReputacionWrapper>();
		for (Vehiculo vehiculo : listaVehiculo) {
			double reputacion = calificionFleteroAdministra.getCalificacionPromedioFletero(vehiculo.getFletero().getIdPersona());
			listado.add(new VehiculoXReputacionWrapper(vehiculo, reputacion));
		}
		List<TipoVehiculo> listaTipoVehiculo = tipoVehiculoAdministra.getTipoVehiculos();
		
		List<Provincia> listaProvincia= provinciaAdministra.getProvincias();
		model.addObject("listaVehiculo", listado);
		model.addObject("listaProvincia", listaProvincia);
		model.addObject("listaTipoVehiculo", listaTipoVehiculo);
		model.setViewName("busquedaServicios");
		return model;
	}
	
	@RequestMapping(value = "/filtrarBusqueda", method = RequestMethod.POST)
	public ModelAndView listadoVehiculosFiltrados(ModelAndView model, HttpServletRequest request) throws IOException {
		String nombreFletero = request.getParameter("nombreFletero");
		TipoVehiculo tipoVehiculo = tipoVehiculoAdministra.getTipoVehiculo(Integer.parseInt(request.getParameter("tipoVehiculo")));
		Barrio barrio = barrioAdministra.getBarrio(Integer.parseInt(request.getParameter("barrio"))); 
		List<Integer> filtrado = fleteroAdministra.getFleteroPorNombreYBarrio(nombreFletero, barrio);
		List<Vehiculo> vehiculosFiltrados = vehiculoAdministra.getVehiculosPorFleterosYTipoVehiculo(filtrado, tipoVehiculo);
		List<Provincia> listaProvincia= provinciaAdministra.getProvincias();
		List<TipoVehiculo> listaTipoVehiculo = tipoVehiculoAdministra.getTipoVehiculos();
		model.addObject("listaProvincia", listaProvincia);
		model.addObject("listaTipoVehiculo", listaTipoVehiculo);
		List<VehiculoXReputacionWrapper> listado = new LinkedList<VehiculoXReputacionWrapper>();
		for (Vehiculo vehiculo : vehiculosFiltrados) {
			double reputacion = calificionFleteroAdministra.getCalificacionPromedioFletero(vehiculo.getFletero().getIdPersona());
			listado.add(new VehiculoXReputacionWrapper(vehiculo, reputacion));
		}
		model.addObject("listaVehiculo", listado);
		model.setViewName("busquedaServicios");
		return model;
	}
	

}