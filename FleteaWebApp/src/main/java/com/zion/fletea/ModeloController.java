package com.zion.fletea;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Marca;
import com.zion.fletea.domain.Modelo;
import com.zion.fletea.service.ModeloAdministra;
import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Handles requests for the application about page.
 */
@Controller
public class ModeloController {

	//private static final Logger logger = LoggerFactory.getLogger(RegistrarTipoVehiculoController.class);

	@Autowired
	private ModeloAdministra modeloAdministra;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@ModelAttribute("allMarcas")
	public List<Marca> populateMarcas() {
		List<Marca> marcas = modeloAdministra.getAllMarcas();
		return marcas;
	}
	
	@RequestMapping(value = "/registrarModelo", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView model) {
		
		Modelo modelo = new Modelo();
		model.addObject("modelo", modelo);
		model.setViewName("registrarModelo");
		return model;
	}
	
	@RequestMapping(value = "/listadoModelo", method = RequestMethod.GET)
	public ModelAndView listadoModelo(ModelAndView model) throws IOException {
		List<Modelo> listaModelo = modeloAdministra.getModelos();
		model.addObject("listaModelo", listaModelo);
		model.setViewName("listadoModelo");
		return model;
	}
	
	
	@RequestMapping(value = "/guardarModelo", method = RequestMethod.POST)
	public ModelAndView guardarModelo(@ModelAttribute Modelo modelo, BindingResult result) {

		if (modelo.getIdModeloVehiculo() == null || modelo.getIdModeloVehiculo() == 0) {

			System.out.println("P.toString: " + modelo.toString());

			modeloAdministra.guardarModelo(modelo);
			
		} else {
			
			modeloAdministra.updateModelo(modelo);			
		}

		return new ModelAndView("redirect:/listadoModelo");
	}
	
	
	@RequestMapping(value = "/editarModelo", method = RequestMethod.GET)
	public ModelAndView editarModelo(HttpServletRequest request) {
		int idModelo = Integer.parseInt(request.getParameter("idModelo"));
		Modelo modelo = modeloAdministra.getModelo(idModelo);
		ModelAndView model = new ModelAndView("registrarModelo");		
		model.addObject("modelo", modelo);
		return model;
	}
	
	@RequestMapping(value = "/borrarModelo", method = RequestMethod.GET)
	public ModelAndView borrarModelo(HttpServletRequest request) {
		int idModelo = Integer.parseInt(request.getParameter("idModelo"));
		modeloAdministra.borraModelo(idModelo);
		return new ModelAndView("redirect:/listadoModelo");
	}
}