package com.zion.fletea;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Cliente;

/**
 * Handles requests for the application about page.
 */
@Controller
public class LoginController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login() {
		return "login";
	}
	
	@RequestMapping(value = "/loginError", method = RequestMethod.GET)
	public String loginError() {
		return "loginError";
	}
	
	@RequestMapping(value = "/registrarse", method = RequestMethod.GET)
	public String registrarse() {
		return "registrarse";
	}
	
//	@RequestMapping(value = "/bienvenido", method = RequestMethod.GET)
//	public ModelAndView bienvenido(HttpServletRequest request) {
//		String usuario = request.getRemoteUser();
//
//		ModelAndView model = new ModelAndView("bienvenido");
//		model.addObject("usuario", usuario);
//		return model;
//	}
	
	@RequestMapping(value = "/accesoDenegado", method = RequestMethod.GET)
	public String accesoDenegado() {
		return "accesoDenegado";
	}
}