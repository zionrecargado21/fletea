package com.zion.fletea.domain;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "mensaje")
public class Mensaje implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idMensaje")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMensaje;
	
	@Column(name = "texto")
	private String texto;
	
	@Column(name = "fecha")
	private Date fecha;
	
	@Column(name= "idServicio")
	private int idServicio;
	
//	@Column(name = "idPersonaRemitente")
//	private int idPersonaRemitente;
	
//	@Column(name = "idPersonaDestinatario")
//	private int idPersonaDestinatario;
	
	//Nuevo

	@ManyToOne
    @JoinColumn(name = "idPersonaDestinatario")
	private Persona personaDestinatario;
	
	@ManyToOne
    @JoinColumn(name = "idPersonaRemitente")
	private Persona personaRemitente;
	//Fin Nuevo
	
	public int getIdServicio() {
		return idServicio;
	}

// Descomentar luego
//	public int getIdPersonaRemitente() {
//		return idPersonaRemitente;
//	}
//
//	public void setIdPersonaRemitente(int idPersonaRemitente) {
//		this.idPersonaRemitente = idPersonaRemitente;
//	}

// Descomentar luego
//	public int getIdPersonaDestinatario() {
//		return idPersonaDestinatario;
//	}
//
//	public void setIdPersonaDestinatario(int idPersonaDestinatario) {
//		this.idPersonaDestinatario = idPersonaDestinatario;
//	}

	public Persona getPersonaDestinatario() {
		return personaDestinatario;
	}

	public void setPersonaDestinatario(Persona personaDestinatario) {
		this.personaDestinatario = personaDestinatario;
	}

	public Persona getPersonaRemitente() {
		return personaRemitente;
	}

	public void setPersonaRemitente(Persona personaRemitente) {
		this.personaRemitente = personaRemitente;
	}

	public void setIdServicio(int idServicio) {
		this.idServicio = idServicio;
	}

	public Integer getIdMensaje() {
		return idMensaje;
	}

	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date dt) {
		this.fecha = dt;
	}

	@Override
	public String toString() {
		return "Mensaje [idMensaje=" + idMensaje + ", texto=" + texto + ", fecha=" + fecha + ", idServicio="
				+ idServicio + ", personaDestinatario=" + personaDestinatario + ", personaRemitente=" + personaRemitente
				+ "]";
	}




	
	
	
	
	
}