package com.zion.fletea.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "modelovehiculo")
public class Modelo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idModeloVehiculo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idModeloVehiculo;

	@ManyToOne
	@JoinColumn(name = "idMarca")
	private Marca marca;

	@Column(name = "descripcion")
	private String descripcion;

	public Modelo() {

	}

	public Modelo(Integer idModeloVehiculo, String descripcion, Marca marca) {
		super();
		this.idModeloVehiculo = idModeloVehiculo;
		this.descripcion = descripcion;
		this.marca = marca;
	}

	public Integer getIdModeloVehiculo() {
		return idModeloVehiculo;
	}

	public void setIdModeloVehiculo(Integer idModeloVehiculo) {
		this.idModeloVehiculo = idModeloVehiculo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Marca getMarca() {
		return marca;
	}

	public void setMarca(Marca marca) {
		this.marca = marca;
	}

	@Override
	public String toString() {
		return "Modelo [idModeloVehiculo=" + idModeloVehiculo + ", descripcion=" + descripcion + ", marca=" + marca
				+ "]";
	}

}