package com.zion.fletea.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "Dia")
public class Dia implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idDia")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDia;
	
	@Column(name = "nombre")
	private String nombre;
	
	
	public Dia() {
	}

	public Integer getIdDia() {
		return idDia;
	}

	public void setIdDia(Integer idDia) {
		this.idDia = idDia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Dia(Integer idDia, String nombre) {
		super();
		this.idDia = idDia;
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return  nombre;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="idDia")	
	private List<DisponibilidadFletero> disponibilidadFletero;
	

	

}
