package com.zion.fletea.domain;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "servicio")
public class Servicio implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idServicio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idServicio;
	
	@ManyToOne
	@JoinColumn(name = "idPersonaFletero")
	private Fletero fletero;
	
	@ManyToOne
	@JoinColumn(name = "idPersonaCliente")
	private Cliente cliente;
	
	@Column(name = "fechaServicio")
	private Date fechaServicio;
	
	@ManyToOne
	@JoinColumn(name = "idVehiculo")
	private Vehiculo vehiculo;
	
	@ManyToOne
	@JoinColumn(name = "idBarrio")
	private Barrio barrio;
	
	@Column(name = "fechaSolicitud")
	private Date fechaSolicitud;
	
	@Column(name = "calle")
	private String calle;
	
	@Column(name = "numeroCalle")
	private int numeroCalle;
	
	@Column(name = "numeroPiso")
	private String numeroPiso;
	
	@Column(name = "dpto")
	private String dpto;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	
	public Integer getIdServicio() {
		return idServicio;
	}

	public Servicio() {
		// TODO Auto-generated constructor stub
	}

	public void setIdServicio(Integer idServicio) {
		this.idServicio = idServicio;
	}

	public Fletero getFletero() {
		return fletero;
	}

	public void setFletero(Fletero fletero) {
		this.fletero = fletero;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Date getFechaServicio() {
		return fechaServicio;
	}

	public void setFechaServicio(Date fechaServicio) {
		this.fechaServicio = fechaServicio;
	}

	public Vehiculo getVehiculo() {
		return vehiculo;
	}

	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}

	public Barrio getBarrio() {
		return barrio;
	}

	public void setBarrio(Barrio barrio) {
		this.barrio = barrio;
	}

	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(int numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	public String getNumeroPiso() {
		return numeroPiso;
	}

	public void setNumeroPiso(String numeroPiso) {
		this.numeroPiso = numeroPiso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	
	
	
}
