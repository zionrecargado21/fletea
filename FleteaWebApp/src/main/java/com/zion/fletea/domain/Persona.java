package com.zion.fletea.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Persona")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Persona implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idPersona")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPersona;
	private String nombre;
	private String calle;

	private String mailUsuario;
	private String password;

	@ManyToOne
	@JoinColumn(name = "idBarrio")
	private Barrio barrio;

	private Integer numeroCalle;
	private String numeroPiso;
	private Integer dni;
	private String telefono;

	public Persona() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Persona(Integer idPersona, String nombre, String calle, Integer idBarrio, Integer numeroCalle,
			String numeroPiso, Integer dni, String telefono, String mailUsuario, String password) {
		super();
		this.idPersona = idPersona;
		this.nombre = nombre;
		this.calle = calle;

		this.numeroCalle = numeroCalle;
		this.numeroPiso = numeroPiso;
		this.dni = dni;
		this.telefono = telefono;
		this.mailUsuario = mailUsuario;
		this.password = password;
	}

	public Integer getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Integer idPersona) {
		this.idPersona = idPersona;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Barrio getBarrio() {
		return barrio;
	}

	public void setBarrio(Barrio barrio) {
		this.barrio = barrio;
	}

	public Integer getNumeroCalle() {
		return numeroCalle;
	}

	public void setNumeroCalle(Integer numeroCalle) {
		this.numeroCalle = numeroCalle;
	}

	public String getNumeroPiso() {
		return numeroPiso;
	}

	public void setNumeroPiso(String numeroPiso) {
		this.numeroPiso = numeroPiso;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	public String getMailUsuario() {
		return mailUsuario;
	}

	public void setMailUsuario(String mailUsuario) {
		this.mailUsuario = mailUsuario;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Persona [idPersona=" + idPersona + ", nombre=" + nombre + ", calle=" + calle + ", idBarrio="
				+ this.barrio.getNombre() + ", numeroCalle=" + numeroCalle + ", numeroPiso=" + numeroPiso + ", dni="
				+ dni + ", telefono=" + telefono + "]";
	}

}