package com.zion.fletea.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "calificacionfletero")

public class CalificacionFletero {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "idCalificacionFletero")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idCalificacionFletero;
	
	@ManyToOne
	@JoinColumn(name = "idServicio")
	private Servicio servicio;
	
	@Column(name = "cantidadEstrellas")
	private int cantEstrellas;
	private String comentario;
	private Date fecha;
	
	
	public Servicio getServicio() {
		return servicio;
	}
	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}
	public int getCantEstrellas() {
		return cantEstrellas;
	}
	public void setCantEstrellas(int cantEstrellas) {
		this.cantEstrellas = cantEstrellas;
	}
	public String getComentario() {
		return comentario;
	}
	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getIdCalificacionFletero() {
		return idCalificacionFletero;
	}
	public void setIdCalificacionFletero(int idCalificacionFletero) {
		this.idCalificacionFletero = idCalificacionFletero;
	}
	
	

}
