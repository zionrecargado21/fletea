package com.zion.fletea.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Provincia")
public class Provincia implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idProvincia")
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer idProvincia;
	
	@Column(name = "nombre")
	private String nombre;

	public Provincia() {
		super();
	}

	public Integer getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Provincia [idProvincia=" + idProvincia + ", nombre=" + nombre + "]";
	}

}
