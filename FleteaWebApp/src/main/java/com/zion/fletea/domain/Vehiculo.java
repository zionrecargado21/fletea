package com.zion.fletea.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "vehiculo")
public class Vehiculo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idVehiculo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idVehiculo;

	@ManyToOne
	@JoinColumn(name = "idTipoVehiculo")
	private TipoVehiculo tipoVehiculo;

	@ManyToOne
	@JoinColumn(name = "idModeloVehiculo")
	private Modelo modeloVehiculo;

	@Column(name = "capacidadCarga")
	private int capacidadCarga;

	@Column(name = "tarifaPorHora")
	private double tarifaPorHora;

	@Column(name = "patente")
	private String patente;

	@Column(name = "a�o")
	private int a�o;

	@ManyToOne
	@JoinColumn(name = "idPersona")
	private Fletero fletero;

	public int getIdVehiculo() {
		return idVehiculo;
	}

	public TipoVehiculo getTipoVehiculo() {
		return tipoVehiculo;
	}

	public void setTipoVehiculo(TipoVehiculo tipoVehiculo) {
		this.tipoVehiculo = tipoVehiculo;
	}

	public Modelo getModeloVehiculo() {
		return modeloVehiculo;
	}

	public void setModeloVehiculo(Modelo modeloVehiculo) {
		this.modeloVehiculo = modeloVehiculo;
	}

	public int getCapacidadCarga() {
		return capacidadCarga;
	}

	public void setCapacidadCarga(int capacidadCarga) {
		this.capacidadCarga = capacidadCarga;
	}

	public String getPatente() {
		return patente;
	}

	public void setPatente(String patente) {
		this.patente = patente;
	}

	public void setIdVehiculo(int idVehiculo) {
		this.idVehiculo = idVehiculo;
	}

	public double getTarifaPorHora() {
		return tarifaPorHora;
	}

	public void setTarifaPorHora(double tarifaHora) {
		this.tarifaPorHora = tarifaHora;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public Fletero getFletero() {
		return fletero;
	}

	public void setFletero(Fletero fletero) {
		this.fletero = fletero;
	}
	

	@Override
	public String toString() {
		return "Vehiculo [idVehiculo=" + idVehiculo + ", tipoVehiculo=" + tipoVehiculo + ", capacidadCarga="
				+ capacidadCarga + ", tarifaHora=" + tarifaPorHora + ", matricula=" + patente + ", a�o=" + a�o
				+ ", persona=" + fletero + "]";
	}

}
