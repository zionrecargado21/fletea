package com.zion.fletea.domain;


import java.sql.*;
import javax.servlet.http.HttpServletRequest;

public class UsuarioLogueado {


	   
	public static int getId(HttpServletRequest request) {
		   int id = -1;
		   String mailUsuario = request.getRemoteUser();
	
		   Connection conn = null;
		   PreparedStatement  stmt = null;
		   try{	
		      Class.forName("com.mysql.jdbc.Driver");		   
		      conn = DriverManager.getConnection(Constantes.DB_URL, Constantes.USER, Constantes.PASS);
		      
		      String sql = "SELECT idPersona FROM persona WHERE mailUsuario = ?";	
		      stmt = conn.prepareStatement(sql);		      		      	      
		      stmt.setString(1, mailUsuario);
		      
		      ResultSet rs = stmt.executeQuery();		      		      
		      rs.next();
		      id  = rs.getInt("idPersona");
		      
		      rs.close();		   
		   }catch(Exception e){
			  id = -1;		      
		   }finally{		     
		      try{
		         if(stmt!=null)
		        	 stmt.close();
		      }catch(SQLException se){
		      }
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		      }
		   }	   
		   return id;
	}
	   
	public static String getRol(HttpServletRequest request) {
		   String rol = "";
		   String mailUsuario = request.getRemoteUser();
	
		   Connection conn = null;
		   PreparedStatement  stmt = null;
		   try{	
		      Class.forName("com.mysql.jdbc.Driver");		   
		      conn = DriverManager.getConnection(Constantes.DB_URL, Constantes.USER, Constantes.PASS);
		      
		      String sql = "SELECT dtype FROM persona WHERE mailUsuario = ?";	
		      stmt = conn.prepareStatement(sql);		      		      	      
		      stmt.setString(1, mailUsuario);
		      
		      ResultSet rs = stmt.executeQuery();		      		      
		      rs.next();
		      rol  = rs.getString("dtype");
		      
		      rs.close();		   
		   }catch(Exception e){
			  rol = "";		      
		   }finally{		     
		      try{
		         if(stmt!=null)
		        	 stmt.close();
		      }catch(SQLException se){
		      }
		      try{
		         if(conn!=null)
		            conn.close();
		      }catch(SQLException se){
		      }
		   }	   
		   return rol;
	}
	
}



