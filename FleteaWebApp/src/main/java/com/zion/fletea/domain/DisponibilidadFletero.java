package com.zion.fletea.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "DisponibilidadFletero")
@IdClass (IdDisponibilidadFletero.class) 
public class DisponibilidadFletero {
	
	@Id
	@ManyToOne
	@JoinColumn(name = "idPersona")
	private Persona fletero;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "idDia")
	private Dia dia;
	
	@Id
	private Integer horaDesde;
	
	@Id
	private Integer horaHasta;
	
	
	
	public DisponibilidadFletero() {
		
	}
	public Persona getFletero() {
		return fletero;
	}
	public void setFletero(Persona fletero) {
		this.fletero = fletero;
	}
	public Dia getDia() {
		return dia;
	}
	public void setDia(Dia dia) {
		this.dia = dia;
	}
	public Integer getHoraDesde() {
		return horaDesde;
	}
	public void setHoraDesde(Integer horaDesde) {
		this.horaDesde = horaDesde;
	}
	public Integer getHoraHasta() {
		return horaHasta;
	}
	public void setHoraHasta(Integer horaHasta) {
		this.horaHasta = horaHasta;
	}
	
	@Override
	public String toString() {
		return "DisponibilidadFletero [fletero=" + fletero.getIdPersona() + ", dia=" + dia.getIdDia() + ", horaDesde=" + horaDesde + ", horaHasta=" + horaHasta
				+ "]";
	}
	
	
	

}
