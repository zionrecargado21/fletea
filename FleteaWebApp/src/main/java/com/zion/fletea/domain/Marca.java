package com.zion.fletea.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "marcavehiculo")
public class Marca implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idMarcaVehiculo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMarca;
	
	@Column(name = "descripcion")
	private String descripcion;
	
	
	public Marca() {
	}

	public Integer getIdMarca() {
		return idMarca;
	}

	public void setIdMarca(Integer idMarca) {
		this.idMarca = idMarca;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Marca(Integer idMarca, String descripcion) {
		super();
		this.idMarca = idMarca;
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return  descripcion;
	}
	
	@OneToMany(cascade=CascadeType.ALL)
	@JoinColumn(name="idMarca")	
	private List<Modelo> modelos;
	

	
	
	

}
