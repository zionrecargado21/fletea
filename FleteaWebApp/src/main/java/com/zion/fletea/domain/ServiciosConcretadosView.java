package com.zion.fletea.domain;

import java.util.Date;

public class ServiciosConcretadosView {
  
  private Integer idServicio;
  private String nombre;
  private String tipoVehiculo;
  private String localidad;
  private String provincia;
  private String fechaSolicitud;
  
  
  
  public ServiciosConcretadosView() {
    super();
    // TODO Auto-generated constructor stub
  }
  
  public Integer getIdServicio() {
    return idServicio;
  }
  public void setIdServicio(Integer idServicio) {
    this.idServicio = idServicio;
  }
  public String getNombre() {
    return nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public String getTipoVehiculo() {
    return tipoVehiculo;
  }
  public void setTipoVehiculo(String tipoVehiculo) {
    this.tipoVehiculo = tipoVehiculo;
  }
  public String getLocalidad() {
    return localidad;
  }
  public void setLocalidad(String localidad) {
    this.localidad = localidad;
  }
  public String getProvincia() {
    return provincia;
  }
  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }
  public String getFechaSolicitud() {
    return fechaSolicitud;
  }
  public void setFechaSolicitud(String fechaSolicitud) {
    this.fechaSolicitud = fechaSolicitud;
  }
  
  

}