package com.zion.fletea.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "serviciotrazabilidad")
public class TrazabilidadServicio implements Serializable {

	@Id
	@ManyToOne
	@JoinColumn(name = "idServicio")
	private Servicio servicio;

	@Id
	@ManyToOne
	@JoinColumn(name = "idEstadoServicio")
	private EstadoServicio estadoServicio;

	@Id
	@Column(name = "fecha")
	private Date fecha;

	public TrazabilidadServicio() {

	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public EstadoServicio getEstadoServicio() {
		return estadoServicio;
	}

	public void setEstadoServicio(EstadoServicio estadoServicio) {
		this.estadoServicio = estadoServicio;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

}
