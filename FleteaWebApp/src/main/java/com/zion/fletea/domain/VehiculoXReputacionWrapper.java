package com.zion.fletea.domain;

public class VehiculoXReputacionWrapper {
	private Vehiculo vehiculo;
	private double reputacion;
	
	
	
	public VehiculoXReputacionWrapper(Vehiculo vehiculo, double reputacion) {
		super();
		this.vehiculo = vehiculo;
		this.reputacion = reputacion;
	}
	
	public Vehiculo getVehiculo() {
		return vehiculo;
	}
	public void setVehiculo(Vehiculo vehiculo) {
		this.vehiculo = vehiculo;
	}
	public double getReputacion() {
		return reputacion;
	}
	public void setReputacion(double reputacion) {
		this.reputacion = reputacion;
	}
	
	

}
