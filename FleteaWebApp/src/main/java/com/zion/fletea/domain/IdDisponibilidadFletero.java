package com.zion.fletea.domain;

import java.io.Serializable;

public class IdDisponibilidadFletero implements Serializable{

	
		 private Persona fletero;  
		 private Dia dia;
		 private Integer horaDesde;
		 private Integer horaHasta;
		   
		 public IdDisponibilidadFletero(Persona fletero, Dia dia, Integer horaDesde, Integer horaHasta) {  
		  super();  
		  this.fletero = fletero;  
		  this.dia = dia;  
		  this.horaDesde = horaDesde;
		  this.horaHasta = horaHasta;
		 }
		 
		 



		public IdDisponibilidadFletero() {
			super();
			// TODO Auto-generated constructor stub
		}





		public Persona getFletero() {
			return fletero;
		}





		public void setFletero(Persona fletero) {
			this.fletero = fletero;
		}





		public Dia getDia() {
			return dia;
		}





		public void setDia(Dia dia) {
			this.dia = dia;
		}





		public Integer getHoraDesde() {
			return horaDesde;
		}

		public void setHoraDesde(Integer horaDesde) {
			this.horaDesde = horaDesde;
		}

		public Integer getHoraHasta() {
			return horaHasta;
		}

		public void setHoraHasta(Integer horaHasta) {
			this.horaHasta = horaHasta;
		}  
		 
		 
		
}
