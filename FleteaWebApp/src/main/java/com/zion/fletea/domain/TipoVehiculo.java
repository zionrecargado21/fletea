package com.zion.fletea.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tipovehiculo")
public class TipoVehiculo implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idTipoVehiculo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTipoVehiculo;
	
	@Column(name = "descripcion")
	private String descripcion;

	public TipoVehiculo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Integer getIdTipoVehiculo() {
		return idTipoVehiculo;
	}

	public void setIdTipoVehiculo(Integer idTipoVehiculo) {
		this.idTipoVehiculo = idTipoVehiculo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "TipoVehiculo [idTipoVehiculo=" + idTipoVehiculo + ", descripcion=" + descripcion + "]";
	}
	
	

}
