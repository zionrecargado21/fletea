package com.zion.fletea.domain;

public class RankingFleterosView {

  private Integer cantServicios;
  private String nombre;
  private String localidad;
  private String provincia;
  
  
  
  public RankingFleterosView() {
    super();
    // TODO Auto-generated constructor stub
  }
  public Integer getCantServicios() {
    return cantServicios;
  }
  public void setCantServicios(Integer cantServicios) {
    this.cantServicios = cantServicios;
  }
  public String getNombre() {
    return nombre;
  }
  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
  public String getLocalidad() {
    return localidad;
  }
  public void setLocalidad(String localidad) {
    this.localidad = localidad;
  }
  public String getProvincia() {
    return provincia;
  }
  public void setProvincia(String provincia) {
    this.provincia = provincia;
  }
  
  
}