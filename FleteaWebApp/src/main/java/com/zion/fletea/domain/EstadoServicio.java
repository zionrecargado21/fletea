package com.zion.fletea.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="estadoservicio")
public class EstadoServicio implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idEstadoServicio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idEstadoServicio;
	
	@Column(name = "descripcion")
	private String descripcion;

	
	public EstadoServicio() {

	}

	public Integer getIdEstadoServicio() {
		return idEstadoServicio;
	}

	public void setIdEstadoServicio(Integer idEstadoServicio) {
		this.idEstadoServicio = idEstadoServicio;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	

	

}
