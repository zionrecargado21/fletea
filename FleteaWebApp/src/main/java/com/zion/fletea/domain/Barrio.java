package com.zion.fletea.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "barrio")
public class Barrio implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "idBarrio")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idBarrio;

	@Column(name = "nombre")
	private String nombre;

	@ManyToOne
	@JoinColumn(name = "idLocalidad")
	private Localidad localidad;

	public Barrio() {

	}

	public Integer getIdBarrio() {
		return idBarrio;
	}

	public void setIdBarrio(Integer idBarrio) {
		this.idBarrio = idBarrio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	@Override
	public String toString() {
		return "Barrio [nombre=" + nombre + "]";
	}

	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}



	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "idPersona")
	private List<Persona> personas;
}
