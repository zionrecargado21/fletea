package com.zion.fletea;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Mensaje;
import com.zion.fletea.domain.Persona;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.UsuarioLogueado;
import com.zion.fletea.service.MensajeAdministra;
import com.zion.fletea.service.ServicioAdministra;

/**
 * Handles requests for the application about page.
 */
@Controller
public class MensajeController {
	
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private MensajeAdministra mensajeAdministra;
	
	
	@Autowired
	private ServicioAdministra servicioAdministra;
	
	
	@RequestMapping(value = "/misMensajes", method = RequestMethod.GET)
	public ModelAndView listadoMensajes(HttpServletRequest request) {
		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);
		
		int usuarioLogueadoId =  UsuarioLogueado.getId(request);
		
		if(usuarioLogueadoId == servicio.getCliente().getIdPersona() || usuarioLogueadoId == servicio.getFletero().getIdPersona() ) {

			List<Mensaje> mensajes = mensajeAdministra.getMensajes(idServicio);
			Persona p = new Persona();
			p.setIdPersona(usuarioLogueadoId);
			System.out.println("P.toString: " + servicio.toString());
			System.out.println("P.toString: " + mensajes.toString());
			System.out.println(p.getIdPersona());
			ModelAndView model = new ModelAndView("misMensajes");
			model.addObject("listaMensajes", mensajes);
			model.addObject("servicio", servicio);
			model.addObject("usuario",p);
			return model;
			
		} else {
			
			//Cambiar ac� a la pagian 405
//			ModelAndView model = new ModelAndView("aaaa");
//			return model;
			return new ModelAndView("redirect:/accesoDenegado");

			
		}
	}
	
	@RequestMapping(value = "/guardarMensaje", method = RequestMethod.POST)
	public ModelAndView guardarMensaje(@ModelAttribute Mensaje mensaje, BindingResult result,HttpServletRequest request) {
		
		if (mensaje.getIdMensaje() == null || mensaje.getIdMensaje() == 0) {
			
			int idLector = Integer.parseInt(request.getParameter("idLector"));
			Date dt = new Date();
			mensaje.setFecha(dt);
			System.out.println("P.toString: " + mensaje.toString());
			Servicio servicio = servicioAdministra.getServicio(mensaje.getIdServicio());
			if(UsuarioLogueado.getId(request) == servicio.getCliente().getIdPersona()) {
				mensaje.setPersonaDestinatario(servicio.getFletero());
				mensaje.setPersonaRemitente(servicio.getCliente());
			} else {
				mensaje.setPersonaDestinatario(servicio.getCliente());
				mensaje.setPersonaRemitente(servicio.getFletero());
			}
			
			mensajeAdministra.guardarMensaje(mensaje);
			
		}

//		return new ModelAndView("redirect:/misMensajes?idServicio=" + mensaje.getIdServicio() + "&idLector=" + mensaje.getIdPersonaRemitente());
//		return new ModelAndView("redirect:/misMensajes?idServicio=" + mensaje.getIdServicio() + "&idLector=" + mensaje.getPersonaRemitente().getIdPersona());
		return new ModelAndView("redirect:/misMensajes?idServicio=" + mensaje.getIdServicio());
		
		
		
	}
	
}