package com.zion.fletea;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Marca;

import com.zion.fletea.service.MarcaAdministra;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Handles requests for the application about page.
 */
@Controller
public class MarcaVehiculoController {

	//private static final Logger logger = LoggerFactory.getLogger(RegistrarTipoVehiculoController.class);

	@Autowired
	private MarcaAdministra marcaAdministra;
	

	@RequestMapping(value = "/registrarMarcaVehiculo", method = RequestMethod.GET)
	public ModelAndView registrarMarcaVehiculo(ModelAndView model) {
		
		Marca marca = new Marca();
		model.addObject("marca", marca);
		model.setViewName("registrarMarcaVehiculo");
		return model;
	}
	
	@RequestMapping(value = "/listadoMarcaVehiculo", method = RequestMethod.GET)
	public ModelAndView listadoMarcaVehiculo(ModelAndView model) throws IOException {
		List<Marca> listaMarcaVehiculo = marcaAdministra.getMarcas();
		model.addObject("listaMarcaVehiculo", listaMarcaVehiculo);
		model.setViewName("listadoMarcaVehiculo");
		return model;
	}
	
	
	@RequestMapping(value = "/guardarMarcaVehiculo", method = RequestMethod.POST)
	public ModelAndView guardarMarcaVehiculo(@ModelAttribute Marca marca, BindingResult result) {

		if (marca.getIdMarca() == null || marca.getIdMarca() == 0) {

			System.out.println("P.toString: " + marca.toString());

			marcaAdministra.guardarMarca(marca);
			
		} else {
			
			marcaAdministra.updateMarca(marca);
			
		}

		return new ModelAndView("redirect:/listadoMarcaVehiculo");
	}
	
	
	@RequestMapping(value = "/editarMarcaVehiculo", method = RequestMethod.GET)
	public ModelAndView editarMarcaVehiculo(HttpServletRequest request) {
		int idMarca = Integer.parseInt(request.getParameter("idMarca"));
		Marca marca = marcaAdministra.getMarca(idMarca);
		ModelAndView model = new ModelAndView("registrarMarcaVehiculo");		
		model.addObject("marca", marca);
		return model;
	}
	
	@RequestMapping(value = "/borrarMarcaVehiculo", method = RequestMethod.GET)
	public ModelAndView borrarMarcaVehiculo(HttpServletRequest request) {
		int idMarcaVehiculo = Integer.parseInt(request.getParameter("idMarca"));
		
		marcaAdministra.borraMarca(idMarcaVehiculo);	
		
		return new ModelAndView("redirect:/listadoMarcaVehiculo");
	}
}