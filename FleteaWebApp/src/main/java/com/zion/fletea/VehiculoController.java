package com.zion.fletea;


import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Fletero;
import com.zion.fletea.domain.Modelo;
import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.domain.UsuarioLogueado;
import com.zion.fletea.domain.Vehiculo;
import com.zion.fletea.service.FleteroAdministra;
import com.zion.fletea.service.ModeloAdministra;
import com.zion.fletea.service.TipoVehiculoAdministra;
import com.zion.fletea.service.VehiculoAdministra;

/**
 * Handles requests for the application about page.
 */
@Controller
public class VehiculoController {
	
	private static final Logger logger = LoggerFactory.getLogger(VehiculoController.class);
	
	@Autowired
	private VehiculoAdministra vehiculoAdministra;
	
	@Autowired 
	private TipoVehiculoAdministra tipoVehiculoAdministra;
	
	@Autowired
	private ModeloAdministra modeloAdministra;
	
	@Autowired
	private FleteroAdministra fleteroAdministra;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	
	@RequestMapping(value = "/registrarVehiculo", method = RequestMethod.GET)
	public ModelAndView registrarVehiculo(ModelAndView model, HttpServletRequest request) throws IOException {
		logger.info("Registrar Vehiculo!");
		Fletero f = new Fletero();
		int id = UsuarioLogueado.getId(request);
		f= fleteroAdministra.getFletero(id);
		List<TipoVehiculo> listadoTipoVehiculo = tipoVehiculoAdministra.getTipoVehiculos();
		List<Modelo> listadoModeloVehiculo = modeloAdministra.getModelos();
		model.addObject("fletero", f);
		model.addObject("listadoTipoVehiculo", listadoTipoVehiculo);
		model.addObject("listadoModeloVehiculo", listadoModeloVehiculo);
		model.setViewName("registrarVehiculo");
		return model;
	}
	
	@RequestMapping(value = "/guardarVehiculo", method = RequestMethod.POST)
	public ModelAndView guardarVehiculos(@ModelAttribute Vehiculo vehiculo, BindingResult result, HttpServletRequest request) {

		if (vehiculo == null || vehiculo.getIdVehiculo() == 0) {
			
			System.out.println("V.toString: " + vehiculo.toString());
			int id = UsuarioLogueado.getId(request);
			Fletero fletero= fleteroAdministra.getFletero(id);
			vehiculo.setFletero(fletero);
			TipoVehiculo tipoVehiculo= tipoVehiculoAdministra.getTipoVehiculo(Integer.parseInt(request.getParameter("tipoVehiculo")));
			Modelo modelo = modeloAdministra.getModelo(Integer.parseInt(request.getParameter("modeloVehiculo")));
			vehiculo.setModeloVehiculo(modelo);
			vehiculo.setTipoVehiculo(tipoVehiculo);
			vehiculoAdministra.guardarVehiculo(vehiculo);
		} else {
			vehiculoAdministra.actualizarVehiculo(vehiculo);
		}

		return new ModelAndView("redirect:/listadoVehiculo");
	}
	
	@RequestMapping(value = "/borrarVehiculo", method = RequestMethod.GET)
	public ModelAndView borrarVehiculo(HttpServletRequest request) {
		int idVehiculo = Integer.parseInt(request.getParameter("idVehiculo"));
		vehiculoAdministra.borrarVehiculo(idVehiculo);
		return new ModelAndView("redirect:/listadoVehiculo");
	}
	
	@RequestMapping(value = "/listadoVehiculo", method = RequestMethod.GET)
	public ModelAndView listadoVehiculo(ModelAndView model, HttpServletRequest request) {
		logger.info("Listado Vehiculo!");
		int id = UsuarioLogueado.getId(request);
		Fletero fletero= fleteroAdministra.getFletero(id);
		List<Vehiculo> listadoVehiculos = vehiculoAdministra.getVehiculosPorFleteroList(fletero.getIdPersona());
		model.addObject("fletero", fletero );
		model.addObject("listadoVehiculo", listadoVehiculos);
		model.setViewName("listadoVehiculo");
		return model;
	}
	
	
}