package com.zion.fletea;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Administrador;
import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.CalificacionCliente;
import com.zion.fletea.domain.CalificacionFletero;
import com.zion.fletea.domain.Cliente;
import com.zion.fletea.domain.EstadoServicio;
import com.zion.fletea.domain.Fletero;
import com.zion.fletea.domain.Provincia;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.domain.TrazabilidadServicio;
import com.zion.fletea.domain.UsuarioLogueado;
import com.zion.fletea.domain.Vehiculo;
import com.zion.fletea.service.AdministradorAdministra;
import com.zion.fletea.service.BarrioAdministra;
import com.zion.fletea.service.CalificacionClienteAdministra;
import com.zion.fletea.service.CalificacionFleteroAdministra;
import com.zion.fletea.service.ClienteAdministra;
import com.zion.fletea.service.FleteroAdministra;
import com.zion.fletea.service.ProvinciaAdministra;
import com.zion.fletea.service.ServicioAdministra;
import com.zion.fletea.service.ServicioTrazabilidadAdministra;
import com.zion.fletea.service.VehiculoAdministra;

@Controller
public class ServicioController {

	@Autowired
	private ServicioAdministra servicioAdministra;

	@Autowired
	private ProvinciaAdministra provinciaAdministra;

	@Autowired
	private FleteroAdministra fleteroAdministra;

	@Autowired
	private ClienteAdministra clienteAdministra;

	@Autowired
	private VehiculoAdministra vehiculoAdministra;

	@Autowired
	private BarrioAdministra barrioAdministra;
	
	@Autowired 
	private AdministradorAdministra administraAdministra;
	
	@Autowired 
	private CalificacionClienteAdministra calificacionClienteAdministra;
	
	@Autowired 
	private CalificacionFleteroAdministra calificacionFleteroAdministra;
	
	@Autowired
	private ServicioTrazabilidadAdministra trazabilidadAdministra;

	@RequestMapping(value = "/solicitudServicio", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request) {
		//int idCliente = Integer.parseInt(request.getParameter("idCliente"));
		int idCliente = UsuarioLogueado.getId(request);
		int idVehiculo = Integer.parseInt(request.getParameter("idVehiculo"));
		Vehiculo vehiculo = vehiculoAdministra.getVehiculo(idVehiculo);
		Servicio servicio = new Servicio();
		servicio.setFletero(vehiculo.getFletero());
		servicio.setCliente(clienteAdministra.getCliente(idCliente));
		//despues sacar
		//servicio.setCliente(clienteAdministra.getCliente(idCliente));
		servicio.setVehiculo(vehiculo);
		request.getSession().setAttribute("servicio1", servicio);
		List<Provincia> provincias = provinciaAdministra.getProvincias();
		ModelAndView model = new ModelAndView("solicitudServicio");
		model.addObject("servicio", servicio);
		model.addObject("provincias", provincias);
		return model;
	}
	
	@RequestMapping(value = "/cambiarEstadoServicio", method = RequestMethod.GET)
	public ModelAndView cambiarEstadoServicio(HttpServletRequest request) {
		int idEstado = Integer.parseInt(request.getParameter("idEstado"));
		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);
		String descEstado;
		
		switch (idEstado) {
		case 1:
			descEstado = "SOLICITADO";
			break;
		case 2:
			descEstado = "CONFIRMADO";
			break;
		case 3:
			descEstado = "CANCELADO";
			break;
		case 4:
			descEstado = "CERRADO";
			break;
		default:
			descEstado = "SOLICITADO";
			break;
		}
		
		TrazabilidadServicio trazabilidad = new TrazabilidadServicio();
		EstadoServicio estado = new EstadoServicio();
		estado.setIdEstadoServicio(idEstado);
		estado.setDescripcion(descEstado);
		trazabilidad.setServicio(servicio);
		trazabilidad.setEstadoServicio(estado);
		trazabilidad.setFecha(new Date());
		
		trazabilidadAdministra.guardarServicio(trazabilidad);
		
		return new ModelAndView("redirect:/verServicio?idServicio=" + servicio.getIdServicio());
	}
	
	@RequestMapping(value = "/verServicio", method = RequestMethod.GET)
	public ModelAndView view(HttpServletRequest request) {
		int idPersona = UsuarioLogueado.getId(request);
		int rolPersona = 1;
		Cliente cliente = clienteAdministra.getCliente(idPersona);
		Fletero fletero = fleteroAdministra.getFletero(idPersona);
		Administrador administrador = administraAdministra.getAdministrador(idPersona);
		
		if (cliente != null) {
			rolPersona = 1;
		}
		
		if (fletero != null) {
			rolPersona = 2;
		}
		
		if (administrador != null) {
			rolPersona = 3;
		}
		
		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);
		
		CalificacionFletero calificacionFletero = calificacionFleteroAdministra.getCalificacionFleteroServicio(idServicio);
		CalificacionCliente calificacionCliente = calificacionClienteAdministra.getCalificacionClienteServicio(idServicio);
		
		TrazabilidadServicio trazabilidad = trazabilidadAdministra.getEstadoServicio(idServicio);
		ModelAndView model = new ModelAndView("verServicio");
		model.addObject("servicio", servicio);
		model.addObject("trazabilidad", trazabilidad);
		model.addObject("rolPersona", rolPersona);
		model.addObject("calificacionFletero", calificacionFletero);
		model.addObject("calificacionCliente", calificacionCliente);
		return model;
	}

	@RequestMapping(value = "/guardarServicio", method = RequestMethod.POST)
	public ModelAndView guardarServicio(@ModelAttribute("servicio") Servicio servicio, BindingResult result,
			HttpServletRequest request) throws ParseException {

		int idBarrio = Integer.parseInt(request.getParameter("barrio"));
		Servicio servicio1 = (Servicio) request.getSession().getAttribute("servicio1");
		servicio.setBarrio(barrioAdministra.getBarrio(idBarrio));
		servicio.setFletero(servicio1.getFletero());
		//servicio.setCliente(servicio1.getCliente());
		servicio.setCliente(servicio1.getCliente());
		servicio.setVehiculo(servicio1.getVehiculo());
		servicio.setFechaSolicitud(new Date());
		String aux = request.getParameter("hora");
		String aux2 = request.getParameter("fechaSolicitud");
		// servicio.
		SimpleDateFormat formatter = new SimpleDateFormat("h:mm a YYYY-MM-dd");
		Date date = formatter.parse(aux + " " + aux2);

		if (servicio.getIdServicio() == null || servicio.getIdServicio() == 0) {
			Date dt = new Date();
			servicio.setFechaServicio(date);
			servicio.setFechaSolicitud(dt);
			servicioAdministra.guardarServicio(servicio);
			
			TrazabilidadServicio trazabilidad = new TrazabilidadServicio();
			EstadoServicio estado = new EstadoServicio();
			estado.setIdEstadoServicio(1);
			estado.setDescripcion("SOLICITADO");
			trazabilidad.setServicio(servicio);
			trazabilidad.setEstadoServicio(estado);
			trazabilidad.setFecha(new Date());
			
			trazabilidadAdministra.guardarServicio(trazabilidad);
		}

//		return new ModelAndView("redirect:/verServicio?idCliente=" + servicio.getCliente().getIdPersona()
//				+ "&idFletero=" + servicio.getFletero().getIdPersona() + "&idVehiculo="
//				+ servicio.getVehiculo().getIdVehiculo());
		return new ModelAndView("redirect:/verServicio?idServicio=" + servicio.getIdServicio());

	}
}
