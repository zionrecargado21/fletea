package com.zion.fletea;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.TipoVehiculo;
import com.zion.fletea.service.TipoVehiculoAdministra;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Handles requests for the application about page.
 */
@Controller
public class TipoVehiculoController {

	//private static final Logger logger = LoggerFactory.getLogger(RegistrarTipoVehiculoController.class);

	@Autowired
	private TipoVehiculoAdministra tipovehiculoAdministra;
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/registrarTipoVehiculo", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView model) {
		
		TipoVehiculo tipovehiculo = new TipoVehiculo();
		model.addObject("tipovehiculo", tipovehiculo);
		model.setViewName("registrarTipoVehiculo");
		return model;
	}
	
	@RequestMapping(value = "/listadoTipoVehiculo", method = RequestMethod.GET)
	public ModelAndView listadoTipoVehiculo(ModelAndView model) throws IOException {
		List<TipoVehiculo> listaTipoVehiculo = tipovehiculoAdministra.getTipoVehiculos();
		model.addObject("listaTipoVehiculo", listaTipoVehiculo);
		model.setViewName("listadoTipoVehiculo");
		return model;
	}
	
	
	@RequestMapping(value = "/guardarTipoVehiculo", method = RequestMethod.POST)
	public ModelAndView guardarTipoVehiculo(@ModelAttribute TipoVehiculo tipovehiculo, BindingResult result) {

		if (tipovehiculo.getIdTipoVehiculo() == null || tipovehiculo.getIdTipoVehiculo() == 0) {

			System.out.println("P.toString: " + tipovehiculo.toString());

			tipovehiculoAdministra.guardarTipoVehiculo(tipovehiculo);
			
		} else {
			
			tipovehiculoAdministra.updateTipoVehiculo(tipovehiculo);
			
		}

		return new ModelAndView("redirect:/listadoTipoVehiculo");
	}
	
	
	@RequestMapping(value = "/editarTipoVehiculo", method = RequestMethod.GET)
	public ModelAndView editarTipoVehiculo(HttpServletRequest request) {
		int idTipoVehiculo = Integer.parseInt(request.getParameter("idTipoVehiculo"));
		TipoVehiculo tipovehiculo = tipovehiculoAdministra.getTipoVehiculo(idTipoVehiculo);
		ModelAndView model = new ModelAndView("registrarTipoVehiculo");		
		model.addObject("tipovehiculo", tipovehiculo);
		return model;
	}
	
	@RequestMapping(value = "/borrarTipoVehiculo", method = RequestMethod.GET)
	public ModelAndView borrarProvincia(HttpServletRequest request) {
		int idTipoVehiculo = Integer.parseInt(request.getParameter("idTipoVehiculo"));
		tipovehiculoAdministra.borraTipoVehiculo(idTipoVehiculo);
		return new ModelAndView("redirect:/listadoTipoVehiculo");
	}
}