package com.zion.fletea;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.Fletero;
import com.zion.fletea.domain.Localidad;
import com.zion.fletea.domain.Provincia;
import com.zion.fletea.service.BarrioAdministra;
import com.zion.fletea.service.FleteroAdministra;
import com.zion.fletea.service.LocalidadAdministra;
import com.zion.fletea.service.ProvinciaAdministra;

@Controller
public class FleteroController {

	@Autowired
	private FleteroAdministra fleteroAdministra;

	@Autowired
	private ProvinciaAdministra provinciaAdministra;

	@Autowired
	private BarrioAdministra barrioAdministra;

	@Autowired
	private LocalidadAdministra localidadAdministra;

	@ModelAttribute("allBarrios")
	public List<Barrio> populateBarrios() {
		List<Barrio> barrio = fleteroAdministra.getAllBarrios();
		return barrio;
	}
	
	@RequestMapping(value = "/registrarFletero", method = RequestMethod.GET)
	public ModelAndView home(ModelAndView model) {

		Fletero fletero = new Fletero();
		model.addObject("fletero", fletero);

		List<Provincia> provincias = provinciaAdministra.getProvincias();
		model.addObject("provincias", provincias);

		model.addObject("localidades", null);
		model.addObject("barrios", null);

		model.setViewName("registrarFletero");
		return model;
	}

	@RequestMapping(value = "/listadoFletero", method = RequestMethod.GET)
	public ModelAndView listadoFletero(ModelAndView model) throws IOException {
		List<Fletero> listaFletero = fleteroAdministra.getFleteros();
		model.addObject("listaFletero", listaFletero);
		model.setViewName("listadoFletero");
		return model;
	}

	@RequestMapping(value = "/guardarFletero", method = RequestMethod.POST)
	public ModelAndView guardarFletero(@ModelAttribute Fletero fletero, BindingResult result,
			HttpServletRequest request) {
		
		int idBarrio = Integer.parseInt(request.getParameter("barrio"));
		Barrio barrio = barrioAdministra.getBarrio(idBarrio);
		fletero.setBarrio(barrio);

		if (fletero.getIdPersona() == null || fletero.getIdPersona() == 0) {
			System.out.println("P.toString: " + fletero.toString());
			fleteroAdministra.guardarFletero(fletero);
		} else {
			fleteroAdministra.updateFletero(fletero);
		}

		//return new ModelAndView("redirect:/login");
		return new ModelAndView("redirect:/bienvenido");
	}

	@RequestMapping(value = "/editarFletero", method = RequestMethod.GET)
	public ModelAndView editarFletero(HttpServletRequest request) {

		int idFletero = Integer.parseInt(request.getParameter("idPersona"));
		Fletero fletero = fleteroAdministra.getFletero(idFletero);
		ModelAndView model = new ModelAndView("registrarFletero");
		model.addObject("fletero", fletero);

		List<Provincia> provincias = provinciaAdministra.getProvincias();
		model.addObject("provincias", provincias);

		List<Localidad> localidad = (List<Localidad>) localidadAdministra
				.getAllLocalidadByProvincia(fletero.getBarrio().getLocalidad().getProvincia().getIdProvincia());
		model.addObject("localidades", localidad);

		List<Barrio> barrio = (List<Barrio>) barrioAdministra
				.getAllBarrioByLocalidad(fletero.getBarrio().getLocalidad().getIdLocalidad());
		model.addObject("barrios", barrio);

		return model;
	}

	@RequestMapping(value = "/borrarFletero", method = RequestMethod.GET)
	public ModelAndView borrarFletero(HttpServletRequest request) {
		int idFletero = Integer.parseInt(request.getParameter("idPersona"));
		fleteroAdministra.borraFletero(idFletero);
		return new ModelAndView("redirect:/listadoFletero");
	}

}