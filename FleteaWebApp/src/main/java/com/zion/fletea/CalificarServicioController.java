package com.zion.fletea;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.zion.fletea.domain.Barrio;
import com.zion.fletea.domain.CalificacionFletero;
import com.zion.fletea.domain.Mensaje;
import com.zion.fletea.domain.Servicio;
import com.zion.fletea.service.CalificacionFleteroAdministra;
import com.zion.fletea.service.ServicioAdministra;

/**
 * Handles requests for the application about page.
 */
@Controller
public class CalificarServicioController {

	private static final Logger logger = LoggerFactory.getLogger(CalificarServicioController.class);

	@Autowired
	private ServicioAdministra servicioAdministra;

	@Autowired
	private CalificacionFleteroAdministra calificacionFleteroAdministra;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/calificarFletero", method = RequestMethod.GET)
	public ModelAndView calificarFletero(Model model, HttpServletRequest request) {
		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);

		CalificacionFletero calificacion = new CalificacionFletero();
		calificacion.setServicio(servicio);

		ModelAndView modelo = new ModelAndView("calificarFletero");
		modelo.addObject("calificacion", calificacion);
		return modelo;
	}

	@RequestMapping(value = "/guardarCalificacionFletero", method = RequestMethod.POST)
	public ModelAndView guardarCalificacionFletero(@ModelAttribute CalificacionFletero calificacion,
			BindingResult result, HttpServletRequest request) {

		int idServicio = Integer.parseInt(request.getParameter("idServicio"));
		Servicio servicio = servicioAdministra.getServicio(idServicio);
		calificacion.setServicio(servicio);
		
		if (calificacion.getIdCalificacionFletero() == 0) {
			Date dt = new Date();
			calificacion.setFecha(dt);
			calificacionFleteroAdministra.guardarCalificacionFletero(calificacion);
		}

		return new ModelAndView("redirect:/verServicio?idServicio=" + calificacion.getServicio().getIdServicio());
	}

}