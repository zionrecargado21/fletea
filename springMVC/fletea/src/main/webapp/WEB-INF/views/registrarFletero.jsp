<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Registraci�n Fletero</h3>
			</div>
		</div>
		<br>
		<div class="row">
			<form action="#">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="nombre">Nombre</label>
							<input type="text" name="nombre" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="apellido">Apellido</label>
							<input type="text" name="apellido" class="form-control">
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="password">Password</label>
							<input type="password" name="password" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="confirm_pass">Confirme Password</label>
							<input type="password" name="confirm_pass" class="form-control">
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="email">Email</label>
							<input type="email" name="email" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="telefono">N�mero telefono</label>
							<input type="tel" name="telefono" class="form-control">
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="cuit">Cuit</label>
							<input type="text" name="cuit" class="form-control">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="direccion">Direcci�n</label>
							<input type="text" name="direccion" class="form-control">
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<label for="provincia">Provincia</label>
						<select name="provincia" id="provincia" class="form-control">
							<option value="c�rdoba">C�rdoba</option>
						</select>
					</div>
					<div class="col-md-3">
						<label for="ciudad">Ciudad</label>
						<input type="text" name="ciudad" class="form-control">
					</div>
					<div class="col-md-3"></div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6 text-center">
						<div class="form-group">
							<button type="" class="btn btn-primary">Aceptar</button>
						</div>
					</div>
					<div class="col-md-3"></div>
				</div>
			</form>
		</div>
	</div>
