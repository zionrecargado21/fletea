package ar.com.zion.fletea.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HolaController {

	@RequestMapping("hola")
	public ModelAndView redireccion() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("hola");
		return mv;
	}
}
